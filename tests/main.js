/* Main config. Configures the global options, such as paths, loader options, etc.
 
NOTES:
[1] Do not use shortcuts with name 'smil' or 'css'. why? because there are plugins named 'smil' and 'css'. Otherwise, Curl won't be able to resolve the path of the plugin, and it can't be loaded. That's because Curl thinks that the plugin name (eg. in "smil!timesheet.xml") is a shortcut, and thus it'd resolve the plugin's path to /lib/smil.js, if the shortcut in config is "smil":'/lib/smil', and won't find the plugin. This is not a bug in Curl, it's just that it tries to resolve shortcuts names, because in the plugin name you can also used a shortcut, eg. if you put the plugin "smil" in /lib/smil/load.js, and in config define a shortcut "smil":'/lib/smil', then you can use the plugin using "smil/load!timesheet.xml".

[2] in RequireJS, use 'urlArgs' (*) to work around browser cache of JS files (http://stackoverflow.com/questions/8315088/prevent-requirejs-from-caching-required-scripts). but it looks Curl does not have an option "urlArgs", Is there any other method to do the same in Curl? No, i didn't find it, so I patched Curl.js to implement that option.
  (*) urlArgs: "bust=v2", // "bust=" + (new Date()).getTime() //it gets appended as params to the URL, eg. myfile.js?bust=v2  
Notice if you add a time value to 'urlArgs', the files will never get cached by browser; this value is used only for development, not for production.
	
*/

// added to be able to test my code in browsers that don't support 'console', otherwise the app will simply not work. eg. in FF the 'console' object is disabled by default.
if(typeof console == 'undefined'){
	var console = {};
	console.log= console.error= console.info= console.debug= console.warn= console.trace= console.dir= console.time= console.timeEnd= console.assert= console.profile= function(){};
	window.console= console;
}

curl({
	//define aliases for easy imports of common modules. If I moved their src files to different location, I won't need to update the paths in each module in my app, only their path in here- the loader's config. 

	//Note that baseUrl is relative to index.html file, not to the loader file (eg. curl.js) 
	baseUrl: "https://bitbucket.org/tadp/baselibs/raw/master", 
	//baseUrl: "../../baselibs",
	paths : {
		//IMPORTANT: Do not use aliases named 'css' or 'smil'. why? see note [1]
		//css: 'static/css',
		jquery: 'jquery/jquery-min'
		//,timesheets:'../timesheets/src'
		,timesheets: 'https://bitbucket.org/tadp/timesheets/raw/master/src'
		//,events: 'events/bean'			
	},
	pluginPath: 'loader/plugin'
	
// load the main app file in the dependency list 
}, ['timesheets/main']);

//TODO: load the polyfills that are needed by our application. Warning: I must load them *before* other modules, such as the timesheet engine.  
