/* This module loads timesheets and instantiates timing objects in Timesheet.js engine. 

In this module, The timesheets are in JSON format, not in XHTML. 
----
i associate the smil elements among components. ie. since each view module defines its own timesheet, there is not a page-wide tree of timing nodes. so I assemble the modules' timing instances to form a global hierarchy, ie. link a sub-container with its parent's, ie. this.parentNode. 
		
smilTimeItem() saves each smil object in the target's node. When a view module is instantiated, it receives its 'context' node in the HTML. Given that, I can find if context's node is a target of a smil element. if yes, the saved smil object is parentNode of smil objects in this module being processed.
*/
define(['jquery','./timesheets','lang/aug','events/bean'],function($,engine,aug,E){
			
	function parseTimeContainer(cDef, ctx){ //'cDef' is a 'timeContainer' definition, eg. <excl>,<seq>, or <par>, but in this module it's a JSON object, not a DOM node.
		var container, parentNode;	
		
		//TODO: instead of ctx.extTiming, use $.data() to attach data to DOM node, eg. $(ctx).data('extTiming', ). Timesheet.js engine also uses '.extTiming', need to fix it too [see notes in Timesheet.js]
		//parentNode= ctx.timing; // '.timing' stores a smil obj defined _inline_, while extTiming stores an *array* of smil objs defined in _timesheets_ [see at the end of smilTimeItem()]
		var smilObjs= ctx.extTiming; 
			
		if (!smilObjs || !smilObjs.length){ 
			//create a 'par' time container and store in context
			var myPar= engine.smilTimeElement({items:[],timeContainer:'par'}, null, null);
			ctx.extTiming= smilObjs=[myPar];
			myPar.target= ctx; //why? this is important because to search for selectors in a view, i use the container.parentTarget (eg. see processTimeDefs() in autoupdate.js), but a container defined in timesheets has no 'target' (well, if it's not an <item> too, or if not defined with inline attrs rather than in timesheet), and the engine will try to see if the container's parentNode(s) have a 'target', and will assign the parent's target to the vble container.parentTarget. Also, containers need a parentTarget in order to attach events to them, so i need to give myPar a target so that its child containers have a defined parentTarget.
			
		}
		
		//WARNING: for simplicity, I assume that parentNode is in index 0. extTiming is an array of smil objs, ie. an HTML node can be target of multiple timesheet items. So which object in extTiming[] is the parentNode of this AMD module? I haven't figured it out yet, so for the moment I assume extTiming[] has only 1 item]
		parentNode= smilObjs[0];
	
		//a container defined in a timesheet (vs defined inline) has no targetNode.			
		container= engine.smilTimeElement(cDef, parentNode, null);
		
		//note: this is the container of the HTML fragment of a module. So it might not be right to call container.show(), because this container might be a sub-item of other parent container. Thus, i call show() only if this is a *main* container, not a child container:
		//if(!smilObjs || !smilObjs.length) container.show();	
		

		var parentActive= parentNode.isActive(), 
			parentConverted=false;

		//parentNode is the time node where I store the new containers. parentNode might be an instance of smilTimeItem, instead of a container. In that case I need to convert it to a 'par' container. We know it isn't a container if parentNode.timeContainer is null/undefined. If so, how to convert it to a container? below is a *temp* hack to convert it:
		if(!parentNode.timeContainer){
			aug(parentNode, engine.smilTimeContainer_par.prototype);
			
			//create tdefs.items[] *before* calling smilTimeContainer_par.call(), because that function calls parseTimeNodes(), which reads tdefs.items[]
			parentNode.tdefs.items=[]; 
			parentNode.timeContainer= "par";
			
			engine.smilTimeContainer_par.call(parentNode, parentNode.tdefs, parentNode.parentNode, parentNode.target);
				
			// boolean to indicate that parentNode was initially an item, not a container. This variable is used later to apply solution (1) or (2) as described in the comments below.
			parentConverted= true;
		}
		// add this container's tdefs to the parent's tdefs.items[] (which is a list of TS defs), coz in autoupdate.js I search for all smil objs that imply a scope, and get its TS defs that will be evaluated within that scope.
		parentNode.tdefs.items.push(cDef); 

		//if this is a child container, add the instance to timeNodes[] of parentNode
		parentNode.timeNodes.push(container);
		
		//If parentNode is already active, it'd never start this child container, because it's in the parent's computeTimeNodes() where it sets time_in/out of its child timeNodes, and that func is called when the parentNode is started. solution? depends on case 1 or 2: 
		if(parentActive){
		//1. If parentNode was an smilTimeItem (not a container) and was converted to a 'par' container (in code above), the solution is to call parentNode's show() which has changed when converted to container; show() will run computeTimeNodes() AND start the container's timer (which calls onTimeUpdate()).  
			if(parentConverted)  parentNode.show();
		
		//2. If parentNode was a container, ie. we didn't convert it in this func, the solution is to re-run parent's computeTimeNodes(). That will set time_in/out of its new added child timeNode, and in the next time step (ie. in container's onTimeUpdate() ) the container will show/hide the new timeNode.
			else parentNode.computeTimeNodes();
		}

  } 
  //helper func to convert XML node to JS object. Think whether it should be moved to separate module, eg. /lib/smil/helpers.js. This func is used in the AMD plugin "smil!" to load external timesheets (in /loader/plugin/smil.js) and in the code that parses internal timesheets (in /lib/smil/main.js).  
  //Note: attribute names in HTML are automatically converted to lowercase! this matters when I try to read attrs such as 'timeAction','beginInc', etc. and can't find them! temp hack:
  var attrNames= {timeaction:'timeAction',begininc:'beginInc', repeatdur:'repeatDur', repeatcount:'repeatCount', filldefault:'fillDefault', mediasync:'mediaSync'};
  function xmlToJson(xml) {
		var items, tdefs=[];
		//'xml' can be the 'timesheet' node, or a timeContainer node, eg. 'seq'
		if(xml.nodeName.toLowerCase().indexOf("timesheet")>-1)
			xml= xml.childNodes[0];
		while(xml){
			// Create the return object
			var obj= {};
			
			if(xml.nodeType == 1) { // element
				// do attributes
				var name= xml.nodeName.toLowerCase();
				if(['seq','par','excl'].indexOf(name)>-1)
					obj.timeContainer= name;
				var prop, attr, attrs= xml.attributes;
				for(var i=0;i<attrs.length;i++){
					attr= attrs[i];
					prop= attrNames[attr.name] || attr.name;
					parseStringValue(obj, prop, attr.value);
				}
			} else if (xml.nodeType == 3 || xml.nodeType == 8) { // text or comment
				xml= xml.nextSibling; continue;
			}
			// do children
			if (xml.hasChildNodes()){
				//childNodes might be text nodes (ie. white space), ignore them in that case
				items= xmlToJson(xml.childNodes[0]);
				if(items.length) obj.items= items;
			}
			//if using mediaSync, automatically set begin/end=Infinity as with Event-values
			if(obj.mediaSync){
				obj.begin= obj.end= Infinity;
			}
			
			tdefs.push(obj);
			xml= xml.nextSibling;
		}
		return tdefs;
	};
  
    //compiles 'index()' params to avoid repetition of work when instantiating each time item.
	function fn(parts){
		var args= parts[1].split(','),
	    indexStart=(args.length>1)? parseInt(args[1]): 0;
	    return function(i){  
			return {
				target: document.getElementById(args[0]+ (indexStart+ i)),
				event: parts[2].substr(1),
				index:1
			};//Note that in this case 'target' is never 'undefined', unlike it happens for begin/end events (see getEventList() ). why? coz the 'index(   )' syntax indicates a 'target'.
	    }
	}            
	            
	function fns(list){
		var fnList=[], parts;
		for(var j=0;j<list.length;j++){
		    parts = list[j].split(/index\(([\w-,]+)\)/);
			//always, parts[0] is a not-matched part; the first match is at parts[1]. Also, I know that *always* after a matched part (eg. '(myId,1)') there will be a not-matched part (eg. '.click;')
		    fnList.push( fn(parts) );
		}
		return fnList;
	}
	//end of code to parse 'index(  ) function'
   
    //this func is in timesheets.js too, so i call that one to avoid code duplication
    //function parseTime(str){
	//}
    var parseTime= engine.parseTime;
  
    // To be able to compile the Timesheet defs at the server, in 'event.target' instead of storing a DOM node (ie. document.getElementById(tmp[0]);), i store the target Id, and get the DOM node for that Id when the timesheets def is loaded in the browser. A similar problem happens when target is not defined in the ts defs, because in that case the target would be "this.parentTarget", the problem is that when parseEvents() is called we don't have an instance of smilTimelement, so we can't set the target at that point. This two things will be done in getEventList() in timesheets.js
	function parseEvents(events){
		//var events = [];
		if(!events || !events.length) return [];

		// TODO: look for "+" in eventStr and handle the time offset respect to the time the event fires
		//var eventStrArray = eventStr.split(/;\s*/);
		for (var i = 0; i < events.length; i++) {  
			var tmp = events[i].split(".");
			var target, evt;
			if (tmp.length >= 2) {
				target = tmp[0];//document.getElementById(tmp[0]);
				evt    = tmp[1];
			} else { 
				//target = this.parentTarget;
				evt    = events[i];
			}
			events[i]={
			  target : target,
			  event  : evt
			};
		}
		
		return events;
	}

  
	function parseEvtStr(segment,prop, value){
		var evts=[], index=[], time, 
		tokens= value.split(';');
		
		for(var j=0;j<tokens.length;j++){
			var t= tokens[j];
			if(!t || !t.length) continue;
			
			if(!isNaN(parseFloat(t))) time= parseTime(t);
			else if(t.indexOf('index(')>-1) index.push(t);
			else evts.push(t);
		}
		if(evts.length){
			segment[prop+"Events"]= parseEvents(evts);
		}if(index.length)
			segment[prop+"Fns"]= fns(index);
		segment[prop]= isNaN(time)? Infinity: time; //'time' could be 0, a positive float, Infinity, NaN or 'undefined'. If there was no 'time' value defined in begin/end, we set segment[prop]= Infinity, why?:
		//Answer: otherwise segment[prop] would have the old string, which is an event name. Why 'Infinity' instead of 'undefined' (or 'null')? Let's see begin and end separately:
		//'end': to if you use 'null' it won't work well, coz this.end is assigned null in computeTimeNodes() (because "isNaN(null)" returns false), while if you use 'undefined' then this.end is assigned Infinity [in computeTimeNodes() or onbeginListener()] (because "isNaN(undefined)" returns true); and if this.end is null then this.time_in > this.time_out always, so the node is deactivated immediately after it's activated; That's why it's important that this.time_out=Infinity in this cases.
		//'begin': if 'begin' is 'undefined', by default its time_in is assigned 0 (respect the syncbase) in computeTimeNodes(). So if you defined an event in begin, eg. begin="T0.click", the element would be activated at time 0 too, which is wrong! that's why we need to set Infinity when 'begin' defines events.
		//Conclusion: begin/end can be Infinity not only when developer defines "begin='indefinite'", but also when 'begin' has events and no time value, eg. "begin='T0.click'". When begin is not defined at all (ie. not even events), the default time_in given in computeTimeNodes() is begin=0 (in 'par' container). Same thing applies for 'end' attr in onendListener(), but when 'end' is undefined (and no 'dur' defined either), the default time_out given in computeTimeNodes() is the container's dur (in 'par') or time_in/out of siblings (in seq/excl).
		
	}
  
	function parseStringValue(segment, attr, value){
		
		switch(attr){
			//case "timeContainer":
			case "timeAction":
			  segment[attr]= value.toLowerCase(); break;
			case "repeatCount":
			  segment[attr]= (value == "indefinite") ? Infinity: parseFloat(value); break;
			case "begin": case "end":
			   //parse begin/end events and index() function
			   parseEvtStr(segment,attr, value); break;
			// time (float or DOM event)
			case "beginInc": case "dur": case "repeatDur":
			  segment[attr]= parseTime(value); break;
			// event
			case "onbegin": case "onend":
			  segment[attr]= function() { eval(value); }; break;
			case "first": case "next": case "prev":case "last":
				segment[attr]= parseEvents(value.split(';')); break;
			case "select":
				//the 'select' attr might have a '>', which is scaped when put in XML docs
				segment[attr]= value.replace('&gt;','>'); break;
			default: 
				// else, it's string or unsupported
				segment[attr]= value;
		}
	}

	return {
		//defs: parsed timesheet definitions, in JSON format.
		//ctx: context node to search for the CSS selectors.
		load:function(defs, ctx){
			//ACHTUNG: this func must be called after the DOM has loaded. I should use domReady() (a micro lib) to wrap this func to ensure it waits until DOM loads, however I don't do it because I used domReady to load the main app module, so I know that the DOM has loaded when this module loads.

			//the JSON data may have multiple container elements.
			for(var i=0;i<defs.length;i++){
				parseTimeContainer(defs[i],ctx);
			}
			
			var ctxContainer=ctx.extTiming[0];
			if(!ctxContainer.parentNode){
				ctxContainer.show();
			}
			//careful, this event will be propagated to parent nodes of 'ctx' node. Is there an option in jQuery's trigger() to not bubble the event? no.

			E.fire(ctx,"SMILContentLoaded");

		},
		xmlToJson: xmlToJson
	};
});
