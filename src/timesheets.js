/*  
//The engine assumes that HTML5 Media API is available, and the app must make sure to provide a polyfill when is not supported in a browser. This way the engine's code is cleaner, smaller, and faster in modern browsers. 

TODO:
 - Code refactoring in smilTimeItem, smilTimeContainer_base, and specific containers
 - use $.data() to save timeNodes to DOM to avoid known memory leaks
  
Code based on the Timesheets.js engine implementation at:
http://wam.inrialpes.fr/timesheets/

*/


/*****************************************************************************\
|                                                                             |
|  SMIL/Timing and SMIL/Timesheet implementation                              |
|                                                                             |
|*****************************************************************************|
|  Utilities:                                                                 |
|                                                                             |
|    checkHash()                          hyperlinks activation           |
|    smil[In|Ex]ternalTimer               timers                    |
|                                                                             |
|-----------------------------------------------------------------------------|
|  SMIL Objects:                                                              |
|                                                                             |
|    smilTimeItem                         base class for SMIL items           |
|    smilTimeContainer_base               abstract class for SMIL containers  |
|                                         inherits smilTimeItem               |
|    smilTimeContainer_[par|seq|excl]     classes for SMIL containers      |
|                                         inherits smilTimeContainer_base  |
|    smilTimeElement                      creates SMIL elements           |
|-----------------------------------------------------------------------------|
|                                                                             |
|  Public API:                                                                |
|                                                                             |
|    document.createTimeContainer(domNode, parentNode, targetNode, timerate)  |
|    document.getTimeNodesByTarget(node)                                      |
|    document.getTimeContainersByTarget(node)                                 |
|    document.getTimeContainersByTagName(tagName)                             |
|                                                                             |
\*****************************************************************************/

define(['events/bean','selector/qwery'], function(E,$){

  // 'MediaContentLoaded' is fired when all media elements have been parsed
  E.onMediaReady = function(callback) {
    E.on(window, "MediaContentLoaded", callback);
  };
 // 'SMILContentLoaded' is fired when all time containers have been parsed
  E.onSMILReady = function(callback) {
    //EVENTS.bind(window, "SMILContentLoaded", callback);
    E.on(document.body,"SMILContentLoaded", function(e){
		if(e.target==this) //DA: i use this check because all custom events buble, and I don't see an option in jQuery's trigger() to say not buble the event. Otherwise, this callback is called multiple times, coz "SMILContentLoaded" event is fired in each view too, not only when done loading the global timesheet.
			callback();
		});
  };

// note: all lines containing "consoleLog" will be deleted by the minifier.
//TODO: console.log setup should be moved outside the engine. ie. these funcs (consoleLog and consoleWarn) 
var DEBUG = true;                             // consoleLog
function consoleLog(message) {                // consoleLog
  if (DEBUG && (typeof(console) == "object")) // consoleLog
    console.log(message);                     // consoleLog
}                                             // consoleLog
function consoleWarn(message) {
  if (typeof(console) == "object")
    console.warn(message);
}

//Why do I need this function? See http://stackoverflow.com/questions/1595611/
function makeSubclass(base) {
    _makeSubclass.prototype= base.prototype;
    return new _makeSubclass();
}
function _makeSubclass() {};

// default timeContainer refresh rate = 40ms (25fps)
var TIMERATE = 40;

// array to store all time containers
var TIMECONTAINERS = [];

// Detect Internet Explorer 6/7/8
// these browsers don't support XHTML, <audio|video>, addEventListener...
var OLDIE = (window.addEventListener) ? false : true;
// var IE6 = (window.XMLHttpRequest) ? false : true;

//TODO: I could put parseTime() in a separate module(eg. in /utils/time.js)? ...or in the global scope(like parseFloat(), etc)? because it might be needed in other modules too. eg. I use it in load_definitions.js too.
var timeUnits={ms:0.001,s:1,min:60,h:3600};
function parseTime(str){
	if(!str || !str.length)  return undefined;
	else if(str == "indefinite")  return Infinity;
    var m = str.match(/(ms|s|min|h)[\s]*$/);
    if(m && m[1]) //unit=m[1]
      return parseFloat(str)*timeUnits[m[1]];
    //'str' can be in [hh:mm:ss] format(eg. "1:45" or "1:5:20"), or a single number (in seconds)
    m= ("0:0:"+str).match(/([0-9]{1,2}:?){3}$/g); //'null' if no match
    if(m) 	return Date.parse("1970/01/01 "+ m[0] )*.001;   
	//else, return 'undefined', not the 'str' value
}

/* DA: Another approach for parseTime(). Test which approach performs faster.
function parseTime(timeStr) {
		if (!timeStr || !timeStr.length)      return undefined;
		else if (timeStr == "indefinite")     return Infinity;
		else if (   /ms[\s]*$/.test(timeStr)) return parseFloat(timeStr) / 1000;
		else if (    /s[\s]*$/.test(timeStr)) return parseFloat(timeStr);
		else if (  /min[\s]*$/.test(timeStr)) return parseFloat(timeStr) * 60;
		else if (    /h[\s]*$/.test(timeStr)) return parseFloat(timeStr) * 3600;
		else if (/^[0-9:\.]*$/.test(timeStr)) { // expecting [hh:mm:ss] format
			var seconds = 0;
			var tmp = timeStr.split(":");
			for (var i = 0; i < tmp.length; i++)
			  seconds = (seconds * 60) + parseFloat(tmp[i]);
			return seconds;
		} else return timeStr; // unsupported time format -- maybe a DOM event?
							 // note that isNaN("string") returns true
}
*/

// ===========================================================================
// Activate a time node if a hash is found in the URL
// ===========================================================================
// This func implements a feature of the SMIL Timing spec:
// http://www.w3.org/TR/SMIL3/smil-timing.html#Timing-HyperlinkImplicationsOnSeqExcl

function checkHash() {
  var targetElement = null; // target DOM node
  var targetTiming  = null; // target time node
  var container     = null; // ???
  var i, tmp;

  // get the URI target element
  var hash = document.location.hash;
  if (hash.length) {
    consoleLog("new hash: " + hash); //eg. hash= "#/bookmarks"
    var targetID = hash.substr(1).replace(/^\/|\&.*$/, ""); //DA: we remove any additional params passed in the hash after the targetId, indicated by the symbol '&', eg. "#targetId&t=2s"
    // the hash may contain a leading char (e.g. "_") to prevent scrolling. DA: to prevent scrolling? in case the browser scrolls *automatically* to the element that has the ID indicated in the URL, eg. http://localhost/mypage.html#targetId
    targetElement = document.getElementById(targetID)
                 || document.getElementById(targetID.substr(1));
  }
  if (!targetElement) return;
  consoleLog(targetElement);

  // get the target time node (if any)
  tmp = document.getTimeNodesByTarget(targetElement);
  if (tmp.length) {
    targetTiming = tmp[0];
    container = tmp[0].parentNode;
  }
  // the hash might contain some temporal MediaFragment information
  var time = NaN;
  if(targetTiming && targetTiming.timeContainer) {
    tmp = hash.split("&");
    for (i = 0; i < tmp.length; i++) {
      if (/^t=.*/i.test(tmp[i])) { // drop end time (if any). the time in the hash can have two parts, separated by a comma, and we must ignore the second part.
        //time = targetTiming.parseTime(tmp[i].substr(2).replace(/,.*$/, ""));
        time = parseTime(tmp[i].substr(2).replace(/,.*$/, ""));
        break;
      }
    }
  }

  // activate the time container on the target element:
  // http://www.w3.org/TR/SMIL3/smil-timing.html#Timing-HyperlinkImplicationsOnSeqExcl
  // we're extending this to all time containers, including <par>
  // -- but we still haven't checked wether '.selectIndex()' works properly with <par>
  var containers = [];
  var indexes    = [];
  var timeNodes  = [];
  var element = targetElement;
  
  while (container) {
    for (var index = 0; index < container.timeNodes.length; index++) {
      if (container.timeNodes[index].target == element) {
        consoleLog("target found: " + element.nodeName + "#" + element.id);
        if (!container.timeNodes[index].isActive()) {
          containers.push(container);
          indexes.push(index);
          timeNodes.push(container.timeNodes[index]);
        }
        break;
      }
    }
    // loop on the parent container. DA: why? coz we not only need to activate the element given in the hash, but also its containers, because an element won't be activated if its container is not activated.
    element   = container.parentTarget; //container.getNode();
    container = container.parentNode;
  }

  for (i = containers.length - 1; i >= 0; i--) {
	container= containers[i];
	 
    if(container.time_in==Infinity)
		container.time_in= container.parentNode.getCurrentTime();
    container.parentNode.selectItem(container);//instead of containers[i].show()
    
    if(container.timeNodes[indexes[i]].time_in==Infinity)
       container.timeNodes[indexes[i]].time_in= container.getCurrentTime();
    
    container.selectIndex(indexes[i]);
  }

  // set the target time container to a specific time if requested (MediaFragment)
  if (targetTiming && !isNaN(time)) {
    targetTiming.setCurrentTime(time);
    consoleLog(targetElement.nodeName + " time: " + time);
  }

  // TODO: ensure the target element is visible. Scroll only if target is not in viewport
  // This is difficult because of conflicts with CSS Transforms, eg. 'slide-in' effect.
  
}
E.onSMILReady(function() {
  consoleLog("SMIL data parsed, starting 'hashchange' event listener.");
  checkHash(); // force to check once at startup
  //EVENTS.onHashChange(checkHash);
  E.on(window, "hashchange", checkHash);
});


/*****************************************************************************\
|                                                                             |
|  smilInternalTimer (                                                        |
|    timerate          : update time in milliseconds (default = 40ms)         |
|  )                                                                          |
|  smilExternalTimer (                                                        |
|    mediaPlayerNode   : <audio|video> node used as time base                 |
|  )                                                                          |
|                                                                             |
|*****************************************************************************|
|                                                                             |
|    .onTimeUpdate     callback function to be triggered on each time update  |
|                                                                             |
|    .isPaused()       returns 'true' when paused                             |
|    .getTime()        returns the current elapsed time, in seconds           |
|                                                                             |
|    .play()           starts playing (and triggering the callback function)  |
|    .pause()          stops playing (and suspend the callback function)      |
|    .stop()           stops playing (and resets the time to zero)            |
|                                                                             |
\*****************************************************************************/

// These two timers implement the same API. Each time container will choose
// the appropriate timer -- internal by default, external when an <audio|video>
// element is in charge of the timing (see the 'syncMaster' SMIL attribute).

function smilInternalTimer(timerate) {
  if (!timerate)
    timerate = TIMERATE; // default = 40 milliseconds timerate (25 fps)

  var self = this;
  
  // read-only properties: isPaused(), getTime()
  var timerID   = null;
  var timeStart = 0;    // milliseconds since 1970/01/01 00:00
  var timePause = 0;    // milliseconds since last play()
  var paused    = true;
  this.isPaused = function() { return paused; };
  this.getCurrentTime  = function() {
    var ms = timePause;
    if (!paused)
      ms += Date.now() - timeStart;
    return (ms / 1000); // returns elapsed time in seconds (float)
  };
  this.setCurrentTime  = function(time) {
	// Added fix to change currentTime even if container is paused 
	if(!paused)
	    timeStart -= (time - self.getCurrentTime()) * 1000;
	else
		timePause = time * 1000;
  };
  //Added to implement requestAnimationFrame() for timing control
  function frameUpdate(time){
	if (paused) return;
	self.onTimeUpdate();
	requestAnimationFrame(frameUpdate);
  }

  // public methods: play(), pause(), stop()
  this.play = function() {
    if (!paused) return;
    timeStart = Date.now();
    paused = false;
    timerID = setInterval(function() { self.onTimeUpdate(); }, timerate);
    //timerID= requestAnimationFrame(frameUpdate);
    //consoleLog("started: " + timerID);
  };
  this.pause = function() {
    if (paused) return;
    clearInterval(timerID);
    timerID = null;
    timePause = 1000 * self.getCurrentTime();
    paused = true;
    self.onTimeUpdate();
    //consoleLog("paused: " + timerID);
  };
  this.stop = function() {
    if (!timerID) return;
    clearInterval(timerID);
    timerID = null;
    timePause = 0;
    paused = true;
    self.onTimeUpdate();
    //consoleLog("stopped: " + timerID);
  };

}
// TODO: implement HTML5 Media API syntax for time containers
function smilExternalTimer(mediaNode) {
  var self = this;
   
  // use HTML5 Media Element fallback for non-modern browsers
  var mediaAPI = mediaNode;
  if (mediaNode.mediaAPI) {
    mediaAPI = mediaNode.mediaAPI;
    consoleLog("MediaElement interface found.");
  }

  // check if HTML5 Media API is supported. If not supported, raise an error saying that this browser does not support it, and that you need a Media API fallback.
  if(!mediaAPI.canPlayType)
	  throw new Error("HTML5 Media API is required for 'mediaSync'!");


  // read-only properties: isPaused(), getTime()
  this.isPaused = function() { return mediaAPI.paused; };
  this.getCurrentTime  = function() { return mediaAPI.currentTime;  };
  this.setCurrentTime  = function(time) {
    consoleLog("setting media time to " + time);
    // if media is seeking, wait until it's done in order to set a time
    if (mediaAPI.seeking) {
      consoleLog("seeking");
      function setThisTime() {
        mediaAPI.currentTime=time;
        mediaAPI.removeEventListener("seeked", setThisTime, false);
        consoleLog("  readyState = " + mediaAPI.readyState);
      }
      mediaAPI.addEventListener("seeked", setThisTime, false);
    }
    else try {
      mediaAPI.currentTime=time;
    } catch(e) {
      consoleWarn(e);
      consoleLog("seeking to time=" + time + "...");
      consoleLog("  readyState = " + mediaAPI.readyState);
      function setThisTimeErr() {
        mediaAPI.currentTime=time;
        mediaAPI.removeEventListener("canplay", setThisTimeErr, false);
        consoleLog("  readyState = " + mediaAPI.readyState);
      }
      mediaAPI.addEventListener("canplay", setThisTimeErr, false);
    }
  };
  
  var onTimeUpdate= function(){
	  self.onTimeUpdate();
  }

  // public methods: play(), pause(), stop()
  this.play  = function() {
    consoleLog("starting continuous timeContainer");
    mediaAPI.addEventListener("timeupdate", onTimeUpdate, false);
	//TODO: should a container start the media element? maybe not, i think a container is not supposed to start the media element, it's the media who starts the container (ie. the container is synchronized with the media elm, not the other way around). If we started the container but the media element is not playing, then the container would stop. Does that behavior make sense? [otherwise, uncomment the following line to start the media]
	//mediaAPI.play();
  };
  this.pause = function() {
    mediaAPI.pause(); //useless? confusing?
  };
  this.stop  = function() {
	mediaAPI.removeEventListener("timeupdate", onTimeUpdate, false);
    //TODO: should a container force stop the media element? maybe not, i think a container is not supposed to stop the media element, it's the media who stops the container (ie. the container is synchronized with the media elm, not the other way around). If we stopped the container but the media element is still playing, should the container stop or continue? [otherwise, uncomment the following two lines to stop the media]	 
    //mediaAPI.pause();
    //mediaAPI.currentTime = 0;
    self.onTimeUpdate();
  };
  
}

/*****************************************************************************\
|                                                                             |
|  smilTimeItem (                                                             |
|    domNode   : a JS object with the timing definitions |
|    parentNode: parent timeNode (container) if this is a child item   |
|    targetNode: DOM node associated to the timeNode                          |
|  )                                                                          |
|*****************************************************************************|
|                                                                             |
|    .target                                   target node                    |
|    .setTargetState(state)                    set SMIL target state          |
|       state = "idle"   : hasn't run yet                                     |
|       state = "active" : running                                            |
|       state = "done"   : finished running                                   |
|                                                                             |
|    .parentNode                               standard DOM properties        |
|    .previousSibling                                                         |
|    .nextSibling                                                             |
|                                                                             |
|    .timeContainer                            standard SMIL attributes       |
|    .timeAction                                                              |
|    .begin                                                                   |
|    .dur                                                                     |
|    .end                                                                     |
|                                                                             |
\*****************************************************************************/

  function getEventList(self,prop){
	  var evtList=[], origList= self.tdefs[prop]; //origList[] is the array of begin/end events in the JS object (ie. in the Timesheet defs)
	  if(!origList) return evtList;  // given that I return evtList[] here, it means that when there is no 'begin'/'end' events defined for an elemnt, this.beginEvents/endEvents will be an empty array (ie. []), instead of null or undefined!!
	  for(var e,i=0;i<origList.length;i++){
		  e= origList[i];
		  evtList.push({event: e.event, target: document.getElementById(e.target) || self.parentTarget}); //note that e.target could be 'undefined', but that's not a problem because getElementById(undefined) returns 'null'.
	  }
	  return evtList;
  }

// Target handler:
// show/hide target nodes according to the 'timeAction' attribute
smilTimeItem.prototype.newTargetHandler = function(timeAction, target) {
  if (!target) {
    return function(state) {};
  }

  // show/hide target nodes according to the 'timeAction' attribute
  var setTargetState_intrinsic  = function(state) {
    target.setAttribute("smil", state);
  };
  var setTargetState_display    = function(state) {
    target.setAttribute("smil", state);
    target.style.display = (state == "active") ? "block" : "none";
    /* closer to the spec but raises a bunch of issues. Disabled.
    if (!target._smildisplay) { // not initialized yet
      if (window.getComputedStyle)
        target._smildisplay = getComputedStyle(target, null).display;
      else // OLDIE
        target._smildisplay = target.currentStyle.display;
      consoleLog(target.style.display);
    }
    target.style.display = (state == "active") ? target._smildisplay : "none";
    */
  };
  var setTargetState_visibility = function(state) {
    target.setAttribute("smil", state);
    target.style.visibility = (state == "active") ? "visible" : "hidden";
  };
  var setTargetState_style      = function(state) {
    target.setAttribute("smil", state);
    var active = (state == "active");
    if (!target._smilstyle) // not initialized yet
      target._smilstyle = target.style.cssText;
    target.style.cssText = active ? target._smilstyle : "";
  };
  var setTargetState_class      = function(state) {
    target.setAttribute("smil", state);
    var active = (state == "active");
    if (!target._smilclass_active)  { // not initialized yet
      var activeCN = target.className + (target.className.length ? " " : "")
                   + timeAction.replace(/class:[\s]*/, "");
      target._smilclass_active = activeCN;
      target._smilclass_idle = target.className;
    }
    target.className = active ? target._smilclass_active
                              : target._smilclass_idle;
  };

  // return the appropriate target handler
  switch (timeAction) {
    case "display":
      return setTargetState_display;
      break;
    case "visibility":
      return setTargetState_visibility;
      break;
    case "style":
      return setTargetState_style;
      break;
    case "intrinsic":
      if (OLDIE) // (!window.XMLHttpRequest)
        // IE6 doesn't support attribute selectors in CSS
        // IE7/8 do support them but the responsiveness is terrible
        // so we default to timeAction="display" for these old browsers
        return setTargetState_display;
      else
        return setTargetState_intrinsic;
    default:
      if (/^class:/.test(timeAction))
        return setTargetState_class;
      /* enable this to set timeAction="intrinsic" as the default behaviour
      else if (OLDIE) // (!window.XMLHttpRequest)
        // IE6 doesn't support attribute selectors in CSS
        // IE7/8 do support them but the responsiveness is terrible
        // so we default to timeAction="visibility" for these old browsers
        return setTargetState_visibility;
      else
        return setTargetState_intrinsic;
      */
      else // timeAction="display" is a less confusing default behaviour
        return setTargetState_display;
      break;
  }
  return null; // to make jslint happy
};

// Event handlers
smilTimeItem.prototype.addEventListener = function(events, callback) {
  for (var i = 0; i < events.length; i++) {
    var evt = events[i];
    if(evt.target)
      E.on(evt.target, evt.event, callback);
    //consoleLog("event listener on '" + evt.target.nodeName + "' added");
  }
};
smilTimeItem.prototype.removeEventListener = function(events, callback) {
  for (var i = 0; i < events.length; i++) {
    var evt = events[i];
    if (evt.target)  
      E.off(evt.target, evt.event, callback);
    //consoleLog("event listener on '" + evt.target.nodeName + "' removed");
  }
};
smilTimeItem.prototype.dispatchEvent = function(eventType) {
  var func = this["on" + eventType];
  
  if(!this.parentTarget) this.parentTarget=this.target; //temp hack
  E.fire(this.parentTarget, eventType);
  if (func)
    func.call(this.parentTarget);
};

//http://www.w3.org/TR/SMIL3/smil-timing.html#Timing-ResettingElementState
function resetInstanceTimes(item){
	//if(!item.dirty) return;	
    item.time_in= item.cBegin || item.begin;
    item.time_out= item.end;

	item.dirty= false;
}
//TODO: Use this func in computeTimeNodes() to calculate 'active end' in par/excl/seq
function setIntervalEnd(item, syncbase){
	
	//Temp fix: if this time node is synced with media, don't set a time_out. 
	if(item.mediaSyncNode && !(item.tdefs.dur<Infinity)) return;
	
	//segment.tdefs.end could be undefined, Infinity, or a float
	if(item.tdefs.end <Infinity)
	  item.time_out = syncbase + item.tdefs.end;
	else if(item.repeatDur && item.dur!=0)
		item.time_out= item.time_in + item.repeatDur;	  
	else if(item.dur !=undefined)
		item.time_out= item.time_in + (item.dur * item.repeatCount);
	else 
	  item.time_out= Infinity; 

}

function scheduleNextSibling(item){
	//this func is called only if item.dirty=true, ie. if time_in/out are not the scheduled times, eg. time_in/out were triggered by events
	if(!item.nextSibling) return;
	var next= item.nextSibling, 
	offset= (next.tdefs.begin < Infinity)? next.tdefs.begin: 0; 
	next.time_in= item.time_out + offset;
	
	if(next.dur || next.end) 
		next.time_out= next.time_in + (next.dur || next.end);
	
	next.dirty=true;
}

// Constructor: should not be called directly (see smilTimeElement)
function smilTimeItem(domNode, parentNode, targetNode) {
  var self = this;
  this.parentNode      = parentNode;
  this.previousSibling = null;
  this.nextSibling     = null;
  this.timeNodes       = []; 
  
  /** SMIL Targets:
    * .target       = DOM target carrying the 'smil' attribute. can be different from domNode for <item> elements
    * .parentTarget = DOM target used by event handlers. can be different from .target for time containers
    */
  this.tdefs= domNode; //time defs
  this.target  = targetNode; //targetNode might be 'undefined'
  this.parentTarget = this.target;
  var node = this.parentNode;
  while (!this.parentTarget && node) {
    this.parentTarget = node.target;
    node = node.parentNode;
  }

  // http://www.w3.org/TR/SMIL3/smil-timing.html#Timing-IntegrationAttributes
  var timeAction = parentNode ? parentNode.timeAction : "intrinsic";
  this.timeAction    = domNode.timeAction || timeAction; 
  this.timeContainer = domNode.timeContainer || null;

  // http://www.w3.org/TR/SMIL/smil-timing.html#Timing-BasicTiming
  //this.begin = domNode.begin; //it could be 'undefined'
  //this.end   = domNode.end;
  this.dur	= domNode.dur; 

  // http://www.w3.org/TR/SMIL3/smil-timing.html#Timing-fillAttribute
  var fillDefault = parentNode ? parentNode.fillDefault : "remove";
  this.fill        = domNode.fill || fillDefault;
  this.fillDefault = domNode.fillDefault || null;

  // http://www.w3.org/TR/SMIL/smil-timing.html#Timing-repeatSyntax
  this.repeatCount = domNode.repeatCount || 1;
  this.repeatDur   = domNode.repeatDur;

  // show/hide target nodes according to the 'timeAction' attribute
  // 'setTargetState' should be considered as a protected method
  this.setTargetState = smilTimeItem.prototype.newTargetHandler
                                    .call(this, this.timeAction, this.target);
  // event handlers
  this.onbegin    = domNode.onbegin;
  this.onend      = domNode.onend;
  
  this.beginEvents = getEventList(this,'beginEvents');//this.parseEvents(this.begin);
  this.endEvents   = getEventList(this,'endEvents');//this.parseEvents(this.end);
  
  //TODO: see my notes about ideas to optimize this function	
  function onbeginListener() {
	var parent= self.parentNode; 
    consoleLog("onbeginListener");
    
    //What's the point in changing time_in/out? Answer: time_in/out correspond to the instance times which define a new scheduled interval, as explained in SMIL Timing spec.
    self.time_in = parent.getCurrentTime();
    
    //[A0]: Note, A) self.end might have changed when interval's begin is resolved. eg. if begin=Infinity (ie. not resolved yet), and 'dur' is defined, then 'end' is not resolved until the begin event time, so once we resolve begin time then we can calculate end. B) Secondly, even if 'self.begin' was a determined value (ie. not Infinity), given that time_in has been modified I need to update time_out, because time_out value might depend on time_in value (and other parameters), eg. time_out= time_in +dur.
    switch(parent.timeContainer){
		case "seq":
			// if self.begin is Infinity, 'begin' is resolved as 0.
			setIntervalEnd(self, self.time_in - Math.min(self.begin,0));
			break;
		case "excl":
			//add any code specific to excl container
			//here don't call 'break' to execute 'default:' block too
		default:
			setIntervalEnd(self, 0);		
	}
    
    //When changed time_in or time_out, set self.dirty=true to indicate that the original scheduled values were changed, and that we'd need to reset them when the item is hidden [why? to not wrongly start that item at that same time_in in subsequent repetitions (ie. when repeatCount>1)].
    self.dirty= true;
    
	parent.selectItem(self);
  }
  function onendListener() {
    consoleLog("onendListener");
    
    //If the container is 'seq', we need to update time_in/out of next timeNodes. In next timeNode, time_in should not be self.begin, it should be previous sibling's time_out+self.begin, because its self.begin is relative to the previous sibling's 'end', not to the start of the container. 
    // it's in hide() where I update next sibling's time_in/out, but only update the next sibling, not ALL the ones after it. why? coz it could be a waste of time because another user event could happen and invalidate those updated time_in/outs of the next siblings, so it's better to update one by one. [see scheduleNextSiblings() ] 
    self.time_out = self.parentNode.getCurrentTime();
    
    self.dirty=true;
    self.parentNode && self.parentNode.onTimeUpdate();
  }
  
  //DA: added these two lines to be able to bind/unbind evts in autoupdate.js, updateTimeNodes(). is this ok? 
	this.onbeginListener= onbeginListener;
	this.onendListener= onendListener;
	
	// set the initial state for the target node
	this.setTargetState('idle');
	
  // main public methods, exposed to DOM via the 'timing' property
  //TODO: could i put these funcs (show(), hide(), reset()) to the prototype? i think it's possible because they are the same across instances(?), and i'd save in memory for each instance. Also, do the same thing for the analogous funcs in smilTimeContainer_base.
  var state = "";
  this.isActive = function() { return (state == "active"); };
  this.show  = function() {
    if (state == "active") return;
    state = "active";
    if (0) try {
		//DA: actually, shouldn't it be this.target instead of domNode?: (because domNode has no id when using external/internal timesheet, only when using inline attrs; while this.target is the HTML node.) 
      consoleLog(domNode.nodeName + "#" + domNode.id + " -- show()");
    } catch(e) {}
    self.setTargetState(state);
    self.dispatchEvent("begin");
    self.addEventListener(self.endEvents, onendListener);
    self.removeEventListener(self.beginEvents, onbeginListener);
    //if (self.parentNode.timeContainer == "excl") consoleLog("show");
  };
  this.hide  = function() {
    if (state == "done") return;
    
    if(self.parentNode.timeContainer=='seq')
		self.dirty && scheduleNextSibling(self);
    
    state = "done";
    if (0) try {
      consoleLog(domNode.nodeName + "#" + domNode.id + " -- hide()");
    } catch(e) {}
    if (self.fill != "hold" && self.fill!="freeze")
      self.setTargetState(state);
    self.dispatchEvent("end");
    self.addEventListener(self.beginEvents, onbeginListener);
    self.removeEventListener(self.endEvents, onendListener);
    //consoleLog("time node: " + self.tdefs.timeContainer + "/" + self.timeAction + " -- " + state);
	self.dirty && resetInstanceTimes(self);
  };
  this.reset = function() {
    if (state == "idle") return;
    state = "idle";
    if (0) try {
      consoleLog(domNode.nodeName + "#" + domNode.id + " -- reset()");
    } catch(e) {}
    self.setTargetState(state);
    self.addEventListener(self.beginEvents, onbeginListener);
    self.removeEventListener(self.endEvents, onendListener);
    //consoleLog("time node: " + self.getNode().nodeName + "/" + self.timeAction + " -- " + state);
	self.dirty && resetInstanceTimes(self);
  };

  //If this item is a media element (audio, video), setup special handling
  if(targetNode && /^audio|video$/.test(targetNode.nodeName.toLowerCase()))
		setupMediaItem(targetNode, this);


  // attach this object to the target element
  if (targetNode && (domNode.select.length)) { // a timesheet <item> (it leaves out *timesheet* containers)
    // store the timesheet item reference in the 'extTiming' property
    if (!targetNode.extTiming)  targetNode.extTiming = [];
    targetNode.extTiming.push(this);
    consoleLog("extTiming: " + targetNode.nodeName);
  } else if (this.target) { // inline timing. (this includes not only inline 'items', but also *inline* containers, because in inline containers this.target is domNode)
    // store the object reference in the 'timing' property
    targetNode.timing = this;
  } //DA: conclusion, 'extTiming' stores timesheet items, except containers (ie. defined with <excl>,<par>,or <seq>, because they are not visual in the HTML) because they are not associated to a target HTML node. And 'timing' stores inline items, including containers because those are associated to the HTML node in which its inline attrs are defined. Also, note that 'extTiming' can have more than one timing node; ie. this HTML node can be the target of more than one <item> element.
}


function setupMediaItem(node, item){ //arg1 is the DOM node, ie. the timeNode's target node
	E.on(node,'begin',function(){
		//i can't call media's play() before the media is ready.
		if(!(node.readyState>2)){
			function playMedia(){
				node.currentTime=0;
				node.play();
				node.removeEventListener("canplay", playMedia, false);
			}
			node.addEventListener("canplay", playMedia, false);
		}
		else{
			node.currentTime=0;
			node.play();
		} 
	});
	E.on(node,'end',function(){
		node.pause(); //stop
	});
	// handle playback repetition:
	
	if( item.repeatCount > 1 || item.repeatDur ){
		E.on(node, 'timeupdate', function(e){

			//to call this code, 'dur' should be shorter than media's implicit duration. Otherwise, this code will never be called, coz 'time' won't ever be larger than 'dur'
			if(item.repeatCount && item.dur){
				if(this.currentTime>item.dur) node.currentTime=0; //repeat from begining
			}
			//if repeatDur< implicit_duration, then there is no need to *repeat* playback. If repeatDur>implicit_duration, then the 'ended' event will tell us when to repeat. In either case, we don't need to call code inside this else-if.
			else if(item.repeatDur){
		
			}
	    });
	
		E.on(node,'ended',function(e){ //'ended' is an HTML5 Media event
			if(item.isActive()){ 
				node.currentTime=0; //set initial time before play
				node.play(); 
			}
		});
	}
}

//Updates timeNode's state if it was frozen (not if it was 'hold'), ie. if its target.attr('smil')=='active' (because attr('smil') should have been 'done' if the node was not frozen, coz we called timeNodes[currentIndex].hide() some before calling this func). Edit: i don't need to check for attr('smil')=='active', just check for timeNode.fill=='freeze', coz one thing *implies* the other, so no need to check both conditions (ie. if fill='freeze' you know that target.attr('smil') is going to be 'active').
function updateFrozenItem(item){
	if(item && item.fill=="freeze")
		forceDoneState(item,true);
}
// arg2 is only saying if the item is the active one. But in 'par' containers there can be multiple active items, so the if-check ignores arg2 in that case.
//Also, when an item has fill='hold', the if-check ignores arg2 too, so that the code in this func is executed for any timeNode that is hold. 
function forceDoneState(item, selected){
	if((item.fill=="freeze" && selected) || item.fill=="hold"){
		var cIndex= item.currentIndex, noIndex= (cIndex <0);
		item.setTargetState("done");
		//Not only do I need to set the state of this item, but also set state for any child timeNodes of this item that are frozen/hold:
		for(var i=0;i<item.timeNodes.length; i++)
			// In 'par' containers currentIndex=-1. So I don't know which timeNodes are frozen, => always pass 'true' to force setting state 'done' on each timeNode.
			forceDoneState(item.timeNodes[i], noIndex? true: (i==cIndex) );
	}
}

/*****************************************************************************\
|                                                                             |
|  smilTimeContainer_base (                                                |
|    domNode   : a JS object with the timing definitions |
|    parentNode: parent timeNode (container) if this is a child item   |
|    targetNode: DOM node associated to the timeNode                          |
|    timerate  : default timerate for the internal timer (if appliable)       |
|  )                                                                          |
|  extends smilTimeItem (kind of)                                             |
|                                                                             |
|*****************************************************************************|
|                                                                             |
|    .timeNodes                array of all child timesheet nodes             |
|    .parseTimeNodes()         find all time nodes                            |
|    .computeTimeNodes()       initialize all time nodes                      |
|    .getMediaSync()           returns the 'syncMaster' time node, if any     |
|                                                                             |
|    .isPaused()               timer methods -- see smil[In|Ex]ternalTimer()  |
|    .getCurrentTime()                                                        |
|    .setCurrentTime()                                                        |
|    .play()                                                                  |
|    .pause()                                                                 |
|    .stop()                                                                  |
|                                                                             |
|    .repeatCount              standard SMIL attributes                       |
|    .repeatDur                                                               |
|                                                                             |
\*****************************************************************************/

smilTimeContainer_base.prototype= makeSubclass(smilTimeItem);
// Time Sync
smilTimeContainer_base.prototype.getCurrentTime = function() {};
smilTimeContainer_base.prototype.setCurrentTime = function() {};
smilTimeContainer_base.prototype.onTimeUpdate   = function() {};

// This method will return an array of all timeNodes that have a non-null
// timeAction (i.e. where *.timeAction != "none").
smilTimeContainer_base.prototype.parseTimeNodes = function() {
	  var timeNodes = [];
	  var syncMasterNode = null;
	  var segment;

	  // find all time nodes
	  //var children = this.getNode().childNodes;
	  var children = this.tdefs.items;  
	  for (var i = 0; i < children.length; i++) {	
		segment = children[i];
		var targets = [];
		
		if(segment.select){ //it's a timesheet item
			targets = $(segment.select, this.parentTarget);
			//console.log("#targets:"+targets.length);
			// an <item> with child nodes is considered as a <par> container		
			if (segment.items && segment.items.length)
			  segment.timeContainer= "par";						
		  }
		  else {  //inline item (or a time container in timesheets (eg. seq, excl, par)?)
			targets.push(segment);
		  }
					  
		  // push all time nodes
		  for (var j = 0; j < targets.length; j++) {
			var target = targets[j];
			var node = null;
			
			if (segment != target) { // timesheet item  
			  node = smilTimeElement(segment, this, target);
			  var beginInc = segment.beginInc;
			  //TODO: also, should I check that segment.begin!=Infinity?:
			  if (!segment.begin && beginInc)
				node.begin = j * beginInc;
				
			  if(segment.beginFns){
				  if(!node.beginEvents) node.beginEvents=[];
				  for(var k=0;k<segment.beginFns.length;k++)	
					node.beginEvents.push(segment.beginFns[k](j));
			  
			  }if(segment.endFns){
				  if(!node.endEvents) node.endEvents=[];
				  for(var k=0;k<segment.endFns.length;k++)	
					node.endEvents.push(segment.endFns[k](j));
			  }
			}
			else //node with inline attrs
			  node = smilTimeElement(segment, this);
			// set syncMasterNode if found
			if (segment.syncMaster)
			  syncMasterNode = target;
			// ignore this node if it has a null 'timeAction' attribute
			if (node.timeAction != "none")
			  timeNodes.push(node);
			else
			  delete(node);
		  }
	  }

	  // add parentNode, previousSibling, nextSibling -- just in case
	  for (i = 0; i < timeNodes.length; i++) {
		segment = timeNodes[i];
		if (i > 0)
		  segment.previousSibling = timeNodes[i-1];
		if (i < timeNodes.length - 1)
		  segment.nextSibling = timeNodes[i+1];
		segment.parentNode = this;
	  }

	  return {
		timeNodes      : timeNodes,
		syncMasterNode : syncMasterNode
	  };
};
	
// Every time node will get two specific attributes: time_in / time_out.
// These attributes will be initialized wherever possible.
smilTimeContainer_base.prototype.computeTimeNodes = function() {};

// If mediaSync attr is not found in timesheet defs, returns syncMasterNode.
smilTimeContainer_base.prototype.getMediaSync = function(syncMasterNode) {
  // Check the (non-standard) 'mediaSync' attribute:
  // this timeContainer attribute directly points to the master clock,
  // which should be either an <audio> or a <video> element.
  var mediaSyncSelector = this.tdefs.mediaSync; 
  return $(mediaSyncSelector)[0] || syncMasterNode;
};

// <seq|excl> time containers can only show one item at a time,
// so we can use a 'currentIndex' property for these two containers.
smilTimeContainer_base.prototype.currentIndex = -1;

smilTimeContainer_base.prototype.selectIndex = function(index) {
 
  if (this.repeatCount > 1)
	if( (index/this.timeNodes.length) <= this.repeatCount)
    index = index % this.timeNodes.length;
    
  if ((index >= 0) && (index < this.timeNodes.length)
                   && (index != this.currentIndex)) {
    consoleLog("current index: " + this.currentIndex);

    // set the time container's time if possible
    var time = this.timeNodes[index].time_in;
    if (!isNaN(time) && (time < Infinity)) {
      //consoleLog("hashchange: set time to " + time);
      if (this.mediaSyncNode) { // continuous media
        this.setCurrentTime(time + 0.1); // XXX hack
        this.onTimeUpdate();
        return;
      } else
        this.setCurrentTime(time);
    }
    
    // update the target state of all time nodes
    this.timeNodes[index].show();
    for (var i = 0; i < index; i++)
      this.timeNodes[i].hide();
    for (i = index + 1; i < this.timeNodes.length; i++)
      this.timeNodes[i].reset();

    this.currentIndex = index;
    // fire event
    this.dispatchEvent("change");
    consoleLog("new index: " + this.currentIndex);
  }
};
smilTimeContainer_base.prototype.selectItem = function(item) {
  var index = this.timeNodes.indexOf(item);
  this.selectIndex(index);
};

// Constructor: *CANNOT* be called directly, use smilTimeElement instead.
function smilTimeContainer_base(domNode, parentNode, targetNode,timerate) {
  
  if(!this.tdefs) //temp hack: added to support converting smilTimeItem to a container. this if-check avoids calling smilTimeItem() because it was already called when the item instance was created.
  
  smilTimeItem.call(this, domNode,parentNode,targetNode);
  var self = this;

  // parse child nodes and compute start/stop times whenever possible
  consoleLog("  initializing: " + this.timeContainer + " (" + domNode.nodeName + ")");
  var result = this.parseTimeNodes();
  
  // compute container's maximum duration
  if (this.dur == undefined) {
	if (!isNaN(this.end - this.begin))
	  this.dur = this.end - this.begin;
	else
	  this.dur = Infinity;
  }  
  
  //var syncMasterNode = result.syncMasterNode;
  this.timeNodes = result.timeNodes;
//  this.computeTimeNodes();

  if (true) { // consoleLog
    consoleLog("  time container: " + this.timeContainer + " (" + this.tdefs.select + ")");
    for (var i = 0; i < this.timeNodes.length; i++) // consoleLog
      consoleLog("    timeNodes[" + i + "]: " + this.timeNodes[i].time_in + " => " + this.timeNodes[i].time_out);
  } // consoleLog

  // timer
  this.mediaSyncNode = this.getMediaSync(result.syncMasterNode);

  //var timer = null;
  if (this.mediaSyncNode) {
    smilExternalTimer.call(this,this.mediaSyncNode);
  } else {
    smilInternalTimer.call(this,timerate);
  }

  //if using mediaSync, automatically add begin/end Events for this container
  if(this.mediaSyncNode){
	var mediaApi= this.mediaSyncNode.mediaAPI || this.mediaSyncNode; 
	E.on(mediaApi, 'begin', this.onbeginListener);
	E.on(mediaApi, 'end', this.onendListener);
  }

  // Public methods to show/hide/reset time container
  //TODO: the methods show(),hide(), and reset() are quite similar to those in smilTimeItem(). Don't duplicate code in both places! modify these methods to share code with smilTimeItem().
  var state = "";
  this.isActive = function() { return (state == "active"); };
  this.show  = function() {
    if (state == "active") return;
    state = "active";
    
    //computes item's time_in/out when the container becomes active. This approach has 2 advantages: 1) app load time is reduced, and 2) saves time by not calling computeTimeNodes() for containers that never get activated in a session.  
    !self.timeNodes.init && self.computeTimeNodes();
    
    self.play(); 
    self.setTargetState(state);
    
    //Adjusts this container's time when parent container has moved its currentTime forward
	//Edit: don't adjust time if this container is sync'd with media (ie. !self.mediaSyncNode), because even if time offset is millisecs, the media seeks for new time and it can be noticed a small interruption.
	self.parentNode &&  !self.mediaSyncNode && (self.time_in<Infinity) &&
		self.setCurrentTime(self.parentNode.getCurrentTime() - self.time_in);
    
    //consoleLog("timeContainer: " + self.timeContainer + " / " + self.timeAction + " -- " + state);
    self.dispatchEvent("begin");
    self.addEventListener(self.endEvents, self.onendListener);
    self.removeEventListener(self.beginEvents, self.onbeginListener);
    //self.currentIndex = -1;
  };
  this.hide  = function() {
    if (state == "done") return;
   
    if(self.parentNode.timeContainer=='seq')
		self.dirty && scheduleNextSibling(self);
	    
    state = "done";
    self.stop();
    if (self.fill != "hold" && self.fill!="freeze")
      self.setTargetState(state);

    //consoleLog("timeContainer: " + self.timeContainer + " / " + self.timeAction + " -- " + state);
    self.dispatchEvent("end");
    self.addEventListener(self.beginEvents, self.onbeginListener);
    self.removeEventListener(self.endEvents, self.onendListener);
    // do not reset currentIndex in this case
    
    self.dirty && resetInstanceTimes(self);
  };
  this.reset = function() {
    if (state == "idle") return;
    state = "idle";
    
 //   self.stop();
    self.setTargetState(state);

    //consoleLog("timeContainer: " + self.timeContainer + " / " + self.timeAction + " -- " + state);
    self.addEventListener(self.beginEvents, self.onbeginListener);
    self.removeEventListener(self.endEvents, self.onendListener);
    self.currentIndex = -1;

	self.dirty && resetInstanceTimes(self);
  }; 
  
  // DA: Note: <seq|excl> time containers can only show one item at a time, so we use the 'currentIndex' property for these two containers. Its initial value is set to -1 in smilTimeContainer_base's prototype.

  // keep a reference of this timeContainer
  TIMECONTAINERS.push(this);
}


/*****************************************************************************\
|                                                                             |
|  smilTimeContainer_par                                                      |
|  extends smilTimeContainer_base                                          |
|                                                                             |
\*****************************************************************************/
smilTimeContainer_par.prototype= makeSubclass(smilTimeContainer_base);
// Time Sync
smilTimeContainer_par.prototype.computeTimeNodes = function(index1,index2) {
  //var self = this;
  for (var i = index1 ||0; i < (index2||this.timeNodes.length); i++) {
    var segment = this.timeNodes[i];
//    segment.reset();

    // time_in
    if(segment.begin==undefined){ //it could be 0
		if (segment.tdefs.begin != undefined)
		  //segment.begin could be either Infinity or a float
		  segment.begin = segment.tdefs.begin;
		else
		  // I must not set the time_in=0 when 'begin' is not defined. This causes a bug when I insert dynamically new nodes into the DOM after the container already started. fixed: when a time obj does not define a 'begin' attr, the time_in should be: 1) if (container started): time_in= currentTime of container, or 2) else: time_in=0.   
		  segment.begin= this.isActive()? this.getCurrentTime():0;
	}
    if(segment.tdefs.dur==undefined)
		segment.dur= (!segment.play)? Infinity: undefined;

	//time_out
	//Calculates the end of "Active Duration" (AD), and stores it in segment.end. An element stays active during the AD, and deactivates at the end of AD. That means, we don't fire 'end' event at the end of each "Simple Duration"(SD), only at the end of AD.
	//segment.tdefs.end could be undefined, Infinity, or a float 
    if(segment.tdefs.end <Infinity)
      segment.end = segment.tdefs.end;
    else if(segment.repeatDur && segment.dur!=0)
		segment.end= segment.begin + segment.repeatDur;
	else if(segment.dur !=undefined)
		segment.end= segment.begin + segment.dur * segment.repeatCount;
	else 
      segment.end= Infinity;

    //set begin/end of scheduled time interval
    segment.time_in= segment.begin;
    segment.time_out= segment.end;
	
  }
 
  //TODO: implement 'endsync' attr to define the container's dur/end. In a par/excl containers, the "implicit duration" is endsync='last'. This means that I can only find out the container's dur dynamically as items become deactivated. Therefore, while the container's dur is not resolved yet, its time_out is Infinity; and once we find its end (using endsync setting) I'd set its time_out/end . Temp hack:
  if(this.end==Infinity) //if it's Infinity, ie. not resolved
     this.end= this.time_out = this.timeNodes.reduce(function(a, b) {
   		return Math.max(a, b.end) }, 0);
	
	
  //Indicates that timeNode's time_in/out has been computed
  this.timeNodes.init= true;
};

function stopTimeNodes(self){
	// If for a *timeContainer* I define fill="freeze" or "hold", I must not hide its child timeNodes if they are frozen (ie. clearFrozen=false). Otherwise, hide the frozen timeNodes too, calling forceDoneState() for *each* item in timeNodes[], not only for self.currentIndex, coz more than one item of its children could be hold.
	var clearFrozen= (self.fill!="freeze" && self.fill!="hold"),
	cIndex= self.currentIndex, noIndex= (cIndex <0);

	for(var i= 0; i < self.timeNodes.length; i++) {
		var item= self.timeNodes[i];
		//TODO: think of a better approach. the aim is to not bind begin events when this item's container is stopped, ie. we called the container's stop()
		item.hide();
		item.removeEventListener(item.endEvents, item.onendListener);
		clearFrozen && forceDoneState(item, noIndex? true: (i==cIndex));
	}
}
// this func is called by the timer, ie. smil[In/Ex]ternalTimer
smilTimeContainer_par.prototype.onTimeUpdate = function() {
  var time = this.getCurrentTime();
  if (( this.repeatCount > 1 && (time/this.dur) <= this.repeatCount)
     || (this.repeatDur && time < this.repeatDur) )
    time = time % this.dur;

	//when we hide a container, hide() calls its stop(), which calls onTimeUpdate(). So in onTimeUpdate() i check if the container has been deactivated, and if so I must deactivate all its child timeNodes AND unbind begin events, so that the timeNodes don't listen for begin events when the container is not active
	if(!this.isActive()){
		stopTimeNodes(this); return;
	}


  // update the state for all ontime nodes
  for (var i = 0; i < this.timeNodes.length; i++) {
    if (time < this.timeNodes[i].time_in)
      this.timeNodes[i].reset();
    else if (time >= this.timeNodes[i].time_out)
      this.timeNodes[i].hide();
    else
      this.timeNodes[i].show();
  }
};
//Though 'par' container doesn't have index management, i define selectIndex() because it's called within checkHash() to activate an item when clicked on an <a>.
smilTimeContainer_par.prototype.selectIndex = function(index) {
    this.selectItem(this.timeNodes[index]);    
};
smilTimeContainer_par.prototype.selectItem = function(item) {
    var time = item.time_in;
    // set the time container's time if possible
    if (!isNaN(time) && (time < Infinity)) {
      this.setCurrentTime(time);
    }    
    item.show();
};

// Constructor (should not be called directly, see smilTimeElement)
function smilTimeContainer_par(domNode, parentNode, targetNode,timerate) {
  smilTimeContainer_base.call(this, domNode, parentNode,targetNode,timerate);

  // index management is not available on <par> containers
  this.currentIndex = -1;
  
}

/*****************************************************************************\
|                                                                             |
|  smilTimeContainer_excl                                                     |
|  extends smilTimeContainer_base                                          |
|                                                                             |
\*****************************************************************************/
//DA: when a new node is inserted into DOM, we need to update not only the new timeNode, but also the prev Sibling, because the prev Sibling might use the new timeNode's "begin" attr to set its time_out. Notice that this only matters when the new node has *inline* timing attrs, not if timing comes from ext timesheet [why? coz if new timeNode gets the timing info from timesheet, its values will be the same as the timeNode that was in its position before the new one was inserted into timeNodes[]]. If I update prev sibling at same time, this func calls timeNode.reset()  and deactivate the prev sibling if it's active!! I think it should not happen that, and let the sibling node end at the time_out updated according to new node's begin (ie. update its time_out value but don't call reset()). [Note: I bind/unbind the events in autoupdate.js, updateTimeNodes(), so even if the node is active the new end events will work automatically].
smilTimeContainer_excl.prototype= makeSubclass(smilTimeContainer_base);
// Time Sync
smilTimeContainer_excl.prototype.computeTimeNodes = function(index1, index2) {
  var segment = null, tnl= this.timeNodes.length;
  for (i = (index2||tnl)-1; i >= (index1||0); i--) { //reverse order
    segment = this.timeNodes[i];
//    segment.reset(); //DA: see above
	
	// FIXED: for this to work the time nodes must be stored in chronological order. That's why I sort the array timeNodes[] within the excl constructor.

    // time_in
    if(segment.begin==undefined){
		if (segment.tdefs.begin != undefined)
		  segment.begin = segment.tdefs.begin;
		else
		  //default 'begin' value for 'excl' containers is Infinity.
		  segment.begin = Infinity;
	}
    if(segment.tdefs.dur==undefined)
		segment.dur= (!segment.play)? Infinity: undefined;

    // time_out
    //segment.tdefs.end could be undefined, Infinity, or a float
    if(segment.tdefs.end <Infinity)
      segment.end = segment.tdefs.end;
    else if(segment.repeatDur && segment.dur!=0)
		segment.end= segment.begin + segment.repeatDur;
	else if(segment.dur !=undefined)
		segment.end= segment.begin + segment.dur * segment.repeatCount;
	else 
      segment.end= Infinity;
	
	//end of AD depends on next item's time_in
	if((i < tnl-1) )//&& (segment.end>this.timeNodes[i+1].time_in))
		segment.end = Math.min(segment.end, this.timeNodes[i+1].time_in);
    
    //set begin/end of scheduled time interval
    segment.time_in= segment.begin;
    segment.time_out= segment.end;
  }
  
  //Indicates that timeNode's time_in/out has been computed
  this.timeNodes.init= true;


  // activate the first time node if required implicitely
  if (!this.timeNodes[0].time_in) // null, NaN or undefined
    this.timeNodes[0].show();

  // set the Time Container's "dur/end" attributes if undefined
  //Note: if container's dur is undefined, the following lines will set its dur based on its timeNodes, that means that when we want to dynamically add timeNodes to a container, we couldn't do it after the time of the container's dur, coz the container has stopped at that time (right?)[what happens if i add timeNodes to a container that has stopped already? they won't be activated]. Temp hack: define the container's dur= "indefinite" to allow to dynamically add timeNodes to an excl container. Edit: using endsync, I determine the container's dur/end dynamically, as items become deactivated. 
  //Note, in an 'excl' (like in 'par'), items can be activated in any order, so I need to detect the end of the container *dynamically*, using endsync setting.
  //if ((this.tdefs.dur == undefined) ) 
  //  this.dur = this.timeNodes[tnl-1].end;
  
  //TODO: implement 'endsync' attr to define the container's dur/end. In a par/excl containers, the "implicit duration" is endsync='last'. This means that I can only find out the container's dur dynamically as items become deactivated. Therefore, while the container's dur is not resolved yet, its time_out is Infinity; and once we find its end (using endsync setting) I'd set its time_out/end. 
  

};

smilTimeContainer_excl.prototype.onTimeUpdate = function() {
  var time = this.getCurrentTime();

  if (( this.repeatCount > 1 && (time/this.dur) <= this.repeatCount)
     || (this.repeatDur && time < this.repeatDur) )
    time = time % this.dur;

	if(!this.isActive()){
		stopTimeNodes(this); return;
	}

  // are we still in the same time node?
  if (this.currentIndex >= 0) {
    var time_in  = this.timeNodes[this.currentIndex].time_in;
    var time_out = this.timeNodes[this.currentIndex].time_out;
    // note: 'outOfBounds' is false if time_in and time_out are both 'NaN'
    var outOfBounds = (time < time_in) || (time >= time_out);
    if (!outOfBounds)
      return;
    //else
      //this.timeNodes[this.currentIndex].hide();
  }

  // Now we're sure we're not in the same time node.
  // For <excl|seq> nodes, only one element can be active at a time,
  // let's try to find out which.
  var index = -1;
  var active = false;
  for (var i = 0; i < this.timeNodes.length; i++) {
    var segment = this.timeNodes[i];
    var withinBounds = (time >= segment.time_in) && (time < segment.time_out);
    if (time < segment.time_in)
      segment.reset();
    else if (time >= segment.time_out)
      segment.hide();
    else if (withinBounds) {
      //return (this.selectIndex(i));
      if (active) {
        // there's already an active time node
        segment.reset();
      } else {
        active = true;
        // given that a new item becomes activated, update any previous frozen item
        updateFrozenItem(this.timeNodes[this.currentIndex]); // warning: this.currentIndex is -1 in the first run, so timeNodes[-1] is undefined! So i added a check in updateFrozenItem() to avoid error
        segment.show();
        index = i;
      }
    }
  }

  // show the next active item
  if (index >= 0) {
    this.currentIndex = index;
    //this.checkIndex();
    //consoleLog("timeSync_seq, new index: " + index);
  } 

};

// Constructor (should not be called directly, see smilTimeElement)
function smilTimeContainer_excl(domNode, parentNode,targetNode, timerate) {
  smilTimeContainer_base.call(this, domNode, parentNode, targetNode,timerate);
  
  // sort chronologically timeNodes[]
  this.timeNodes.sort(function(a,b){
		return a.begin - b.begin;
  });

  // index management
  this.currentIndex = -1;
  if (this.timeNodes.length && (this.timeNodes[0].time_in <= 0))
    this.currentIndex = 0; // XXX hack for the 'audio.xhtml' demo with Firefox
  setNavigationEvts.call(this, domNode);
}

function setNavigationEvts(domNode){
	var self=this;
	// note that domNode.first (or next, prev,...)could be undefined, in that case it's better to not call addEventListener(), which does not check its arg is undefined 
	//DA: getEventList() reads the list of events for first/prev/etc and does two things: 1) finds the DOM node associated to the targetId (in 'target') for each event, 2) if 'target' is undefined, it uses parentTarget. Note that for the attrs first/prev/etc I don't store in 'this' the event list returned by getEventList(), because I only bind these events once; by contrast, in smilTimeItem, for the attrs 'begin'/'end' I do store in 'this' the events returned by getEventList() coz I bind/unbind those events every time a timeNode is (de)activated.
	domNode.first && this.addEventListener(getEventList(this,'first'), function() {
		self.selectIndex(0);
	});
	domNode.prev && this.addEventListener(getEventList(this,'prev'),  function() {
		self.selectIndex(self.currentIndex - 1);
	});
	domNode.next && this.addEventListener(getEventList(this,'next'),  function() {
		self.selectIndex(self.currentIndex + 1);
	});
	domNode.last && this.addEventListener(getEventList(this,'last'),  function() {
		self.selectIndex(self.timeNodes.length - 1);
	});
	
}

/*****************************************************************************\
|                                                                             |
|  smilTimeContainer_seq                                                      |
|  extends smilTimeContainer_base                                          |
|                                                                             |
\*****************************************************************************/
//DA: when a new node is inserted into DOM, we need to update not only the new timeNode, but also the next Sibling, because the next Sibling might use the new timeNode's "time_out" attr to set its time_in. problem: that will call timeNode.reset() and deactivate the next sibling if it's active!! I think it should not happen that, and let the next sibling node end at its programmed time_out. [Note: I bind/unbind the events in autoupdate.js, updateTimeNodes(), so even if the node is active the new endEvents will work automatically] 

smilTimeContainer_seq.prototype= makeSubclass(smilTimeContainer_base);

// Time Sync
smilTimeContainer_seq.prototype.computeTimeNodes = function(index1,index2) {
  var segment = null, tnl= this.timeNodes.length;
  for (var i = index1||0; i < (index2||tnl); i++) {
    segment = this.timeNodes[i];
//    segment.reset(); //DA: see above

	// time_in
    if(segment.begin==undefined){
		if (segment.tdefs.begin != undefined)
		  segment.begin = segment.tdefs.begin;
		else
		  //in 'seq', using getCurrentTime() here causes a bug when index>0, coz 'segment.begin' is given a very high value when inserting timeNodes dynamically, and cBegin becomes very high because it's the sum begin+timeNodes[i-1].end. That's why I added a check for i==0
		  segment.begin=(i==0 && this.isActive())? this.getCurrentTime():0;
	}
	// "the *syncbase* of the child elements of a seq is the active end of the previous element." ie. the 'begin' attr is the offset respect the previous element's active end, not respect the start time of the container.
	// 'computed begin' (cBegin) is the time relative to the container's begin
	var syncbase= (i==0)? 0: this.timeNodes[i-1].end;
	segment.cBegin= syncbase + segment.begin;
	
    if(segment.tdefs.dur==undefined)
		segment.dur= (!segment.play)? Infinity: undefined;

	//time_out
	//segment.tdefs.end could be undefined, Infinity, or a float
    if(segment.tdefs.end <Infinity)
      segment.end = syncbase + segment.tdefs.end;
    else if(segment.repeatDur && segment.dur!=0)
		segment.end= segment.cBegin + segment.repeatDur;
	else if(segment.dur !=undefined)
		segment.end= segment.cBegin + segment.dur * segment.repeatCount;
	else 
      segment.end= Infinity;
	
    //set begin/end of sheduled time interval
    segment.time_in= segment.cBegin;
    segment.time_out= segment.end;
    
}

  //Indicates that timeNode's time_in/out has been computed
  this.timeNodes.init= true;


  // set the Time Container's "dur/end" attributes if undefined
 
  //TODO: [x] 1)the following two if-statements may be problematic for dynamically adding nodes to DOM, coz a node might be the last node in a container and an instant after it will not. do i need to change this?????   
  //if container's dur is undefined, the following lines will set its dur based on its timeNodes, that means that when we want to dynamically add timeNodes to a container, we couldn't do it after the time of the container's dur, coz the container has stopped at that time (right?)[what happens if i add timeNodes to a container that has stopped already? they won't be activated]. solution: define the container's dur= "indefinite" to allow to dynamically add timeNodes to an excl container. Problem: that's the approach I followed for <excl> container, however in this computeTimeNodes() it checks for "(this.dur>=Infinity)" in the condition to set this.dur a fixed value!! which means I can't use Infinity as in my idea. solution: ??? (remove the check for Infinity in the condition to this.dur??? [think of any unwanted effects if i do such thing!]) 
  
  //if ((this.dur == undefined) || (this.dur >= Infinity)) //DA: why does it also check for "(this.dur>=Infinity)"?? to my understanding, if dur is Infinity it should not have a *determined* value, so I don't know why it sets dur= timeNodes[tnl-1].time_out when it's Infinity (??) 
  if ((this.tdefs.dur == undefined) ) //So i removed the check for dur>=Infinity. The reason is described above. Also, instead of checking for 'this.dur', I check if the tc's timesheet defs has 'dur' attr, why? coz if this func (ie. computeTimeNodes()) ran before, the tc's dur would have been set, so it won't be set in subsequent times i call computeTimeNodes(); that's why I replaced this.dur for this.tdefs.dur
  //Edit: here I'd have to implement 'endsync' attribute. Not only i must check if container's dur is undefined, i should check also for container's end, and repeatDur, and if it's an item in an excl and stops on other item's time_in...
    this.dur = this.timeNodes[tnl-1].end;
/*  if (this.end == undefined) 
    this.end = this.timeNodes[tnl-1].time_out + this.begin;
 */ 
  //TODO: 2)having set this.dur, should I update container's time_out here (since this.dur has been set)?? I won't need to if the child items' times_in/out are computed *before* the container's times_in/out
 
  //TODO: 3)need to update the time_in of the next node when index2<itemNodes.length, ie. when i insert a new node and computeTimeNodes() only updates that node. look at the two lines (copied from above):
  // else if ((i > 0) && (this.timeNodes[i-1].time_out < Infinity))
    //  segment.time_in = this.timeNodes[i-1].time_out;
  
  //TODO: do analogous changes in computeTimeNodes() of <excl> container!!!! (ie. changes 1), 2) and 3) described in this func)
};
smilTimeContainer_seq.prototype.onTimeUpdate = function() {
  var time = this.getCurrentTime();
  var withinBounds, outOfBounds, segment;
  if (( this.repeatCount > 1 && (time/this.dur) <= this.repeatCount)
     || (this.repeatDur && time < this.repeatDur) )
    time = time % this.dur;

	if(!this.isActive()){
		stopTimeNodes(this); return;
	}

	//DA: Problem: when i delete timeNodes from timeNodes[], it won't be possible to use 'currentIndex' to get the timeNode in the array this.timeNodes[]!! coz the number of elements in the array has changed. fixed: when i delete timeNodes from this.timeNodes[] I must change currentIndex accordingly.

  // Step1: are we still in the same time node?
  if (this.currentIndex >= 0) {
    //var time_in  = this.timeNodes[this.currentIndex].time_in;
    //var time_out = this.timeNodes[this.currentIndex].time_out;
    segment = this.timeNodes[this.currentIndex];
    // note: 'outOfBounds' is false if time_in and time_out are both 'NaN'
    outOfBounds  = (time < segment.time_in) || (time >= segment.time_out);
    withinBounds = (time >= segment.time_in) && (time < segment.time_out);
    if (withinBounds)
      return;
    else
      this.timeNodes[this.currentIndex].hide();
  }

  // Now we're sure we're not in the same time node as in prior onTimeUpdate().
  // For <excl|seq> nodes, only one element can be active at a time,
  // let's try to find out which in order to call its show().

  // Step2: There are good chances that the next time node is selected.
  if (this.currentIndex < this.timeNodes.length - 1) {
    var next= this.timeNodes[this.currentIndex + 1];
    // note: 'outOfBounds' is false if time_in and time_out are both 'NaN'
    outOfBounds = (time < next.time_in) || (time >= next.time_out);
    if ((next.time_in >= Infinity) || !outOfBounds) {
		
		if(next.time_in >= Infinity){
			next.time_in = time;
			setIntervalEnd(next, next.time_in);
			next.dirty= true;
		}
		updateFrozenItem(this.timeNodes[this.currentIndex]);
		this.currentIndex++;
		next.show();
		return;
    }
  }

  // Step3: No luck. now we have to search through all time nodes.
  var index = -1;
  var active = false;

  for (var i = 0; i < this.timeNodes.length; i++) {
    segment = this.timeNodes[i];
    withinBounds = (time >= segment.time_in) && (time < segment.time_out);
    if (time < segment.time_in)
      segment.reset();
    else if (time >= segment.time_out)
      segment.hide();
    else if (withinBounds) {
      //return (this.selectIndex(i));
      if (active) {
        // there's already an active time node
        segment.reset();
      } else {
        active = true;
        updateFrozenItem(this.timeNodes[this.currentIndex]);
        segment.show();
        index = i;
      }
    }
  }

  // show the next active item
  if (index >= 0) 
    this.currentIndex = index;
  
 
   //TODO: At the moment, I schedule next siblings when an item becomes hidden, instead of doing it in this place. See my notes for a description of the issue and other proposed ideas.
  else if ((this.currentIndex < this.timeNodes.length - 1))
    //this.selectIndex(this.currentIndex+1);
     console.log("NO ITEM ACTIVE IN SEQ");
};

// Constructor (should not be called directly, see smilTimeElement)
function smilTimeContainer_seq(domNode, parentNode, targetNode,timerate) {
  smilTimeContainer_base.call(this, domNode, parentNode, targetNode,timerate);
  var self = this;

  // index management
  this.currentIndex = -1;
  if (this.timeNodes.length && (this.timeNodes[0].time_in <= 0))
    this.currentIndex = 0; // XXX hack for the 'audio.xhtml' demo with Firefox

  setNavigationEvts.call(this, domNode);
}


/*****************************************************************************\
|                                                                             |
|  smilTimeElement (                                                          |
|    domNode   : node of the SMIL element (in the HTML or Timesheet document) |
|    parentNode: parent time container                                        |
|    timerate  : default timerate for the internal timer (if appliable)       |
|  )                                                                          |
\*****************************************************************************/
//TODO: why to indicate the timerate per timeContainer? the timerate should be an *application-wide* option. Actually, it should be defined in the polyfill for requestAnimationFrame.
function smilTimeElement(domNode, parentNode, targetNode, timerate) {
  var element;
  switch (domNode.timeContainer) {
    case "par":
      element= new smilTimeContainer_par(domNode, parentNode, targetNode,timerate);
      break;
    case "seq":
      element= new smilTimeContainer_seq(domNode, parentNode, targetNode,timerate);
      break;
    case "excl":
      element= new smilTimeContainer_excl(domNode, parentNode, targetNode,timerate);
      break;
    default: // time item
	  // when timeContainer is defined to an unexpected value (eg. "exc" instead of "excl"), it should throw an error (or just a warning?):
	  if(domNode.timeContainer) //it means: timeContainer attr was defined, but is not "par","seq",or "excl"
		  throw ("SMIL Error: '"+ domNode['timeContainer']+"' is not a valid timeContainer!");
		  // console.warn("SMIL Warning: '"+ this.timeContainer+"' is not a valid timeContainer!");
	  //else: it's not a time Container, it's a time item
      element= new smilTimeItem(domNode, parentNode, targetNode);
  }

  // experimental: attach this timeContainer to the HTML node
  if (domNode.timeContainer) { // consoleLog
    //domNode.timing = this;  // consoleLog
    consoleLog("  " + domNode.timeContainer + " (" + domNode.nodeName + ") properly initialized.");
  }                         // consoleLog
  return element;
}

/*****************************************************************************\
|                                                                             |
|  Public API                                                                 |
|  DA: these funcs are not used internally in my code. they are given only 
| for convenience of developers of applications                               |
|*****************************************************************************|
|                                                                             |
|  document.createTimeContainer(domNode, parentNode, targetNode, timerate)    |
|                                                                             |
|  document.getTimeNodesByTarget(node)                                        |
|  document.getTimeContainersByTarget(node)                                   |
|  document.getTimeContainersByTagName(tagName)                               |
|                                                                             |
\*****************************************************************************/

document.createTimeContainer = function(domNode, parentNode, targetNode, timerate) {
  return smilTimeElement(domNode, parentNode, targetNode, timerate);
};
document.getTimeNodesByTarget = function(node) {
  var timeNodes = [];
  if (!node) return timeNodes;
  if (node.timing)    // DA: inline SMIL Timing. Only one timeNode associated to the given node 
    timeNodes.push(node.timing);
  if (node.extTiming) { // SMIL Timesheet. May be multiple timeNodes associated to the node
    for (var i = 0; i < node.extTiming.length; i++)
      timeNodes.push(node.extTiming[i]);
  }
  timeNodes.item = function(index) { return timeNodes[index]; };
  return timeNodes;
};

document.getTimeContainersByTarget = function(node) {
  var contNodes = [];
  var timeNodes = document.getTimeNodesByTarget(node);
  for (var i = 0; i < timeNodes.length; i++) {
    if (timeNodes[i].timeContainer)
      contNodes.push(timeNodes[i]);
  }
  contNodes.item = function(index) { return contNodes[index]; };
  return contNodes;
};

document.getTimeContainersByTagName = function(tagName) {
  var contNodes = [];
  tagName = tagName.toLowerCase();
  if ((/^(par|seq|excl)$/).test(tagName)) {
    for (var i = 0; i < TIMECONTAINERS.length; i++) {
      if (TIMECONTAINERS[i].timeContainer.toLowerCase() == tagName)
        contNodes.push(TIMECONTAINERS[i]);
    }
  } else if (tagName == "*") {
    contNodes = TIMECONTAINERS;
  }
  contNodes.item = function(index) { return contNodes[index]; };
  return contNodes;
};
		
return{
	smilTimeElement: smilTimeElement,
	smilTimeContainer_par: smilTimeContainer_par,
	parseTime: parseTime
	};


});
