
define(['./timesheets','./load_definitions','./autoupdate', 'lang/aug','events/bean','selector/qwery','dom/domReady'],function(engine, loader, autoupdate, aug, E,$,domReady){
	
	
// ===========================================================================
//Edit: my HTML5 Media fallback emulates the HTML5 Media API, so I modified the Timesheets engine to use the native Media API, and thus the two funcs below are not needed anymore.
// ===========================================================================
/*function parseMediaElement(node) {
  // use MediaElement.js when available: http://mediaelementjs.com/
  if (window.MediaElement) {
    var m = new MediaElement(node, {
      success: function(mediaAPI, element) {
        // note: element == node here
        console.log("MediaElement with " + mediaAPI.pluginType + " player");       
        element.mediaAPI      = mediaAPI;
 
        E.fire(document, "MediaElementLoaded");
      },
      error: function() {
        //throw("MediaElement error");
        alert("MediaElement error");
      }
    });
  }
  else { // native HTML5 media element
    //node.pause(); // disable autoplay
    // Why do i need to add the func setCurrentTime() on the media node? Answer:  HTML5 API uses 'currentTime' to set AND get the time, but MediaElement.js API needs to use setCurrentTime(). However, i'm going to create a lib to emulate 100% the HTML5 API, so i'll fix those issues using Object.defineProperty(). thus, this step will become uncessary.
    node.setCurrentTime = function(time) {
      node.currentTime = time;
    };
    
    E.fire(document, "MediaElementLoaded");
  }
}
function parseAllMediaElements() {
	
  var mediaElements = $("audio, video");
  if ( mediaElements.length == 0) {
    // early way out: no <audio|video> element in the current document
    E.fire(window, "MediaContentLoaded");
    return;
  }
  // TODO: check if HTML5 Media API is supported. if not supported, raise an error saying that this browser does not support it, and that you need MediaElement.js as fallback. ie. similar to the check in the code below, but without need to detect if it's IE, just check for a feature that indicates if Media API is supported.
//  else if (OLDIE && !window.MediaElement) {
    // http://mediaelementjs.com/ required. disabled at the moment
//    if (0) throw "MediaElement.js is required on IE<9";
//  }

  var meLength= mediaElements.length, meParsed = 0;
  function countMediaElements() {
    meParsed++;
    if (meParsed >= meLength){
      E.off(document, "MediaElementLoaded", countMediaElements);
      E.fire(window, "MediaContentLoaded");
    }
  }
  E.on(document, "MediaElementLoaded", countMediaElements);

  // initialize all media elements
  for(var i= 0; i < mediaElements.length; i++)
    parseMediaElement(mediaElements[i]);
} */

// ===========================================================================
// Find all time containers in the current document
// ===========================================================================
function parseTimeContainerNode(node) {
  if (!node) return;
  if (!node.timing) {
    consoleLog("Main time container found: " + node.nodeName);
    consoleLog(node);
    // this node's "timing" property isn't set: this node hasn't been parsed yet.
    var smilPlayer = smilTimeElement(node);
    smilPlayer.show();
  } else {
    consoleLog("Child time container found: " + node.nodeName);
  }
}

function parseInlineContainers(){
	  var timeContAttr= "*[data-timecontainer], *[smil-timecontainer], *[timeContainer]"; //DA: why looks for those three attrs? because not sure yet what will be implemented in HTML (ie. in XHTML+Timing it's supported using a namespace, ie. smil:timeContainer, but in HTML there is no namespaces (ie. 'smil:...'), so INRIA proposes two options for HTML5: use smil-timeContainer(better, coz it's more specific for time dimension in HTML docs) or data-timeContainer('data-*' is already in HTML5))
  
	  // Inline Time Containers (HTML namespace)
	  var allTimeContainers = $(timeContAttr);
	  for (var i = 0; i < allTimeContainers.length; i++)
		parseTimeContainerNode(allTimeContainers[i]);

	//DA: finds time containers defined with namespace (ie. in XHTML), ie. 'smil:timeContainer':
	// Inline Time Containers (SMIL namespace) -- we have to use XPath because
    // document.querySelectorAll("[smil|timeContainer]") raises an exception.
    //if (docElt.getAttribute("xmlns") && docElt.getAttribute("xmlns:smil")) {
    var docElt= document.documentElement;
    if (docElt.getAttribute("xmlns")) {
      // the document might have SMIL extensions
     var smilNS = docElt.getAttribute("xmlns:smil") || "http://www.w3.org/ns/SMIL";
     function nsResolver(prefix) { return ((prefix=='smil')? smilNS: null); }

      var containers = document.evaluate("//*[@smil:timeContainer]", document,
                  nsResolver, XPathResult.ORDERED_NODE_ITERATOR_TYPE, null);
      var thisContainer = containers.iterateNext();
      try {
        while (thisContainer) {
          parseTimeContainerNode(thisContainer);
          thisContainer = containers.iterateNext();
        }
      } catch(e) { // Safari tends to raise exceptions here, dunno why
        //consoleLog(e.toString());
      }
    }
	
}
function parseAllTimeContainers() {
  
  // External Timesheets: callback to count all parsed timesheets
  var timesheets = $("link[rel=timesheet]");
  var tsLength = timesheets.length;
  var tsParsed = 0;
  function countTimesheets() {
    tsParsed++;
    if (tsParsed > tsLength)
      E.fire(window, "SMILContentLoaded");
  }

  // External Timesheets: parsing.  
  for (var i = 0; i < tsLength; i++){
		 curl(['smil!'+timesheets[i].href]).then(function(tdefs){
			loader.load(tdefs, document.body); //arg2 is the context for this Timesheet. At the moment, i use 'document.body' (instead of 'document') as context for document-level timesheets [see autoupdate.js for some notes about this issue]. Anyway, i could change the approach and use 'document' instead, if other people think it's more appropriate than 'document.body'.
			countTimesheets();
		},function(){
			//else, can't load the timesheet but still dispatch the related event
			countTimesheets();
		}); 
   }
  // Internet Explorer 6/7/8 don't support XHTML sent as application/xhtml+xml
  // => these browsers won't support internal timesheets nor smil:* attributes
  // => don't use internal timesheets nor smil:* attributes for web content! (DA: that means, you can use internal timesheets or smil:* attrs when you know your users don't use IE (eg. an Intranet), but you should not use them for public web sites because you don't know what browsers users will use)
	//Edit: how to find DOM elements that are namespaced? answer: see http://stackoverflow.com/questions/853740/jquery-xml-parsing-with-namespaces
  
    // Internal Timesheets
    var smilNS = document.documentElement.getAttribute("xmlns:smil");
    timesheets = document.getElementsByTagNameNS(smilNS, "timesheet");
    if (!timesheets.length) // polyglot markup (not working with IE)
      timesheets = document.getElementsByTagName("timesheet");
    tsLength= timesheets.length;
    
    if(tsLength){
		var tdefs=[];
		for(var i= 0; i < tsLength; i++)
		  tdefs= tdefs.concat( loader.xmlToJson(timesheets[i] ));
			
		loader.load(tdefs, document.body);
	}
  //TODO: disabled. I don't support inline timing data yet.
  //parseInlineContainers();

  // for our counter, all internal timing data is considered as one timesheet-- Notice in countTimesheets() the line "if(tsParsed > tsLength)", it uses the symbol '>', so it counts *+1* than tsLength; instead of using '>=' which would stop at tsLength, not at tsLength +1.  
  countTimesheets();
}

	// ===========================================================================
	// Startup: *patch* media elements first, then parse all time containers
	// ===========================================================================
	//onDOMReady() is what starts the parsing of a document, ie. the entry point of the Timesheet.js engine. 
	domReady(function(){
	  console.log("SMIL/HTML Timing: startup");

	  // what it does: when it finished parsing media elemnts, then parse all TimeContainers. why? coz when you create smilTimeElements you will use HTML5 Media API if the node is synchronized with audio or video, that's why first i need to patch the media nodes (audio and video) to support HTML5 Media API in old browsers.
	  //E.onMediaReady(parseAllTimeContainers);
	  //parseAllMediaElements();
	  
	  // Our code here has been simplified because doesn't wait until media is ready. Changes: 1) Using my HTML5 Media fallback it's not necessary to patch media nodes here. 2) I don't need to wait until a Media element's src is completely loaded, because my Media fallback will queue any calls made before the Flash object is ready.
	  parseAllTimeContainers();
			
	}); 

	return aug(engine,loader, autoupdate);

});
