/*

- how developers should load this module?
I added an additional module to import everything (main.js). That is, a module that itself imports timesheets.js and autoupdate.js modules, and returns an object that merges the returned objects from them. That way, we meet the two requirements: 1) the original modules are still separated and developers have the possibility to import them separately or modify them. 2) developers only need to know/load the main.js module, instead of the other two modules separately, so we don't force developers to know about the existance of the individual modules. 

- TODO: when I load a view, in addition to apply the timesheet defined for that view, I should also to apply the timesheet(s) defined for the global scope, which are stored in document.body, eg. $.data(document.body,'timesheets'). That's what the autoupdate.js module does, it not only tries to apply rules from timesheets in the view's scope, but also timesheets from scopes higher in the DOM tree, eg. in document.body.
-----
Note [02]: Approach to find timesheet defs to apply on new DOM nodes:
Given that a selector (in SMIL Timesheets) must be searched within the scope of the current matched target (when defining nested time defs), I think to auto-update when I insert/delete nodes in DOM, I can:
- go up the DOM tree (starting from our node's parent) and stop when you find a node that is a *scope*, ie. it's a target and container (it has nested timesheet defs, items[]) 
- once found a node get its timesheet defs associated to that node, and get its 'select' attr, which will use to find targets in the scope the found node marks
- repeat going up the DOM tree until reach document.body, which is the most top scope.

*/
define(['jquery','./timesheets'],function($, engine){
	
	// listener for nodes added/removed to/from DOM. This is executed only once when the module is loaded.
	$(document).bind('DOMNodeAdded',function(ev,nodes,parent){
		//console.log("node added");
		
		//TODO: we can receive a list of nodes, not just one. note those nodes are consequtive, ie. they are siblings. i need to make changes in this module, specially in the funcs called before updateTimeNodes(). but for the moment (for initial tests), i assume we only get one node, so i pick the first one in nodes[]:
		var node=nodes[0];
		
		
		findTimeDefs(node,parent);
		
	}).bind('DOMNodeDeleted',function(ev, nodes,parent){
		// use the deleted node/s to get its associated timeNode (eg. node.extTiming), and for each time object get its parentNode, and get parentNode.timeNodes[], delete the entry for my node, and then call updateTimeNodes()

		//console.log("nodes removed:"+nodes.length);
		var node, smilObjs,timeNode, tsdefs,index, tc;
		for(var i=0;i<nodes.length;i++){
			node= nodes[i];
			smilObjs= node.extTiming;
			if(!smilObjs) continue;	
			for(var j=0;j<smilObjs.length;j++){
				timeNode= smilObjs[j];
				tc= timeNode.parentNode;
				var timeNodes=	tc.timeNodes;
				tsdefs=timeNode.tdefs;
				//finds which is the timeNode's index/position i must delete in timeNodes[]
				index= timeNodes.indexOf(timeNode);
				timeNodes.splice(index,1); //deletes item
				updateTimeNodes(tsdefs, index, tc, false);
			}
		}
		
	});
		
	function removeIndexEvts(evts){
		for(var i=evts.length-1;i>=0;i--)
			if(evts[i].index) evts.splice(i,1);
	}
	
	function updateTimeNodes(segment, index, tc, insert){
		var timeNodes= tc.timeNodes,
		tnl= timeNodes.length,
		index1= index,index2, i;
	
		for(i=index;i<tnl;i++){ 
			var tn=timeNodes[i];
			
			//A container's timeNodes[], can have timeNodes instantiated from a list of different child <item> elemnts, so Not all timeNodes were created with the same timesheet defs. Thus, when we find timeNodes created with a different timesheet defs, we stop the iteration.
			if(segment!=tn.tdefs) break;
			
			//check if beginInc is defined. if yes, I must recalculate the 'begin' attr in the timeNodes after index, which is the position at which the new time node has been inserted (or deleted)
			if(segment.beginInc)
				tn.begin= segment.beginInc*i;

			//check if index is defined in beginFns/endFns (they are arrays). If yes, update beginEvts/endEvts of the time nodes
			//how to override the invalid evts from beginEvts/endEvts?? [ie. i need to *unbind* the events already set] ie. when timing defs were parsed initially and it had index() in begin/end, some event names were generated with index() and added to beginEvts/endEvts arrays. but now that i re-calculate the index(), I need to override the evts in those arrays that are no longer valid.
			var beginFns= segment.beginFns, endFns= segment.endFns,
			isActive= tn.isActive();
			
			if(beginFns){
				if(!isActive)
					tn.removeEventListener(tn.beginEvents, tn.onbeginListener);					
				removeIndexEvts(tn.beginEvents);//remove invalid events				
				for(var j=0;j<beginFns.length;j++) 
					tn.beginEvents.push(beginFns[j](i));
				if(!isActive) //bind the new events
					tn.addEventListener(tn.beginEvents, tn.onbeginListener);
			}if(endFns){
				if(isActive)
					tn.removeEventListener(tn.endEvents, tn.onbeginListener);					
				removeIndexEvts(tn.endEvents);
				for(var j=0;j<endFns.length;j++) 
					tn.endEvents.push(endFns[j](i));
				if(isActive)
					tn.addEventListener(tn.endEvents, tn.onbeginListener);
			}
		
		//TODO: what if mediaSync is altered by the node inserted/deleted in DOM??
		//this.mediaSyncNode = this.getMediaSync(result.syncMasterNode);

		}
		
		//adjust tc.currentIndex, because the timeNodes[] has been modified, and tc.currentIndex does not point to the same timeNode that it did before the modification.
		if(index<=tc.currentIndex){
			if(insert) tc.currentIndex++;
			else tc.currentIndex--;
		}
		
		// add parentNode, previousSibling, nextSibling -- just in case	
		var timeNode= timeNodes[index];
		if(!timeNode) return;
		if (index > 0){
		  timeNode.previousSibling = timeNodes[index-1];
		  timeNodes[index-1].nextSibling= timeNode;
		}
		if(insert)
		if (index < timeNodes.length - 1){
		  timeNode.nextSibling = timeNodes[index+1];
		  timeNodes[index+1].previousSibling= timeNode;
		}
	
	
		// compute time_in and time_out for each time node whenever possible.
		//computeTimeNodes() will be different depending on the type of this item's container. Note: pass the 'index' to computeTimeNodes(), to not update all the nodes, only those that became dirty. so i modified the func computeTimeNodes() in timesheets.js to receive two args: 'index1' and 'index2'. 
		//if no segment.beginInc, it implies the existing timeNodes have not changed their "begin" property, and thus there is no need to update their time_in, so computeTimeNodes() only needs to update the newly inserted time node. Except, for seq (or excl) containers, computeTimeNodes() will also update the next (or prev) siblings.
		switch(tc.timeContainer){
			case 'seq':
				if(insert || index1>0) index2= tnl; break;
			case 'excl':
				if(index1>0) index1--; //no 'break;' here!
			default: //'par','excl'
				if(segment.beginInc) index2= i;
				else if(insert) index2=index+1;
		}
		(index2-index1)>0 && tc.computeTimeNodes(index1,index2);
			
	}
	function isTargetInCtxt(ctx,target){ //note: 'ctx' and 'target' are both DOM nodes
		var parent= target;
		while(parent){
			if(ctx==parent) return true;
			parent=parent.parentNode;
		}
		return false;
	}
	//gets Timesheet definitions for a given scope
	function getDefsForScope(items, tc, node){ //tc: timeContainer
		var item,defs=[];
		for(var i=0;i<items.length;i++){
			item= items[i];
			//alert("getDefsForScope():"+toSource(item));
			if(item.select) defs.push(item);
		}
		if(defs.length) processTimeDefs(defs, node,tc);	
		
		
		//BUG: in processTimeDefs(), we add a timeNode to tc.timeNodes[]
		
		
		if(tc.timeNodes) {
			for(var obj,j=0;j<tc.timeNodes.length;j++){
				obj= tc.timeNodes[j];
				//<item> defs do have a target, but i'm looking for nodes that don't have a 'select' attr in their TS defs, ie. <seq>,<par>,or <excl> tags, which don't have a target (unless they are the root of the view or the document, but in that case its TS defs would have been already processed)
				if(obj.target) continue; 
				getDefsForScope(obj.tdefs.items,obj, node);
			}	
		}
	}
	
	// what if a segment had initially no time nodes? ie. if a time container had no items in its timeNodes[]. In that case, how do we get the container node? (I need it to create the new instance of the new item, ie. smilTimeElement( )). the container node might not be associated to a DOM node, coz container nodes are not visual, just a logical group.[it'd be associated to a DOM node only if the container is also an item, coz timing items (ie. <item select=' ' />) are always associated to a target DOM node(either the match node of 'select' (for timesheets) or the HTML node that has inline timing attrs).]. So, how do we get the container node?? Solution: store the instance of a time container in the node that marks its scope or in 'document.body', which is the top-most scope. what?! A time container belongs to a scope, which is either 1)the target node of a parent <item>, or 2) the 'document.body' obj when the container has no parent, ie. it's a document's root time container. this excerpt from SMIL Timesheets spec can help you understand:
	//"Item Selection Process for Nested Items: The "second" item elements uses the ".Bullet" CSS class selector to select all the bullets of the presentation. However, the *scope* of the selection is *limited by* the first item element. WIthin the first slide, the CSS selector can match only the descendants of the div element "Slide1"."
	//That is, each target found at a level becomes the scope/context for the search for targets at the child level. In "Figure 1", in section 3.2, each div found in the first level will become the scope to search for the selector in the second level.
	
	//loop thru the timesheet definitions, and check if the definition's 'select' matches any node within the given node [remember the added node is not only 1 node, it can a be the root of a DOM fragment]
	function processTimeDefs(defs, node, tc){
		var segment;
		for(var j=0;j<defs.length;j++){
			segment= defs[j];
			if(!segment.select) continue; // it's a container.
			
			// when you search for a selector within a context, the context node is not included in the search, only its children nodes. That's why I can't just use 'ctx' as context for the search of targets, because 'ctx' itself could be a target. solution: use ctx.parentNode as context (instead of tc.parentTarget) for the search and also use isTargetInCtxt() to filter the found targets.
			
			var targets= $(segment.select, tc.parentTarget);
			if(!targets || targets.length==0) continue; //no targets found at this segment
			for(var pos,i=0;i<targets.length;i++){
				//if(node==targets[i]){ //this is wrong, coz the matched target might not be our new 'node', but a child of node. Instead, i use isTargetInCtxt() to check if target is a node's child.
				if(isTargetInCtxt(node,targets[i])){
					var timeNode = engine.smilTimeElement(segment, tc, targets[i]);
					
					//if it's an excl container, the timeNode must be inserted at the position/index that keeps the chronological order in timeNodes[] array. Let's find the right position:
					if(tc.timeContainer=="excl"){
						var begin= timeNode.begin;
						for(var j=0;j<tc.timeNodes.length;j++){
							if(begin<tc.timeNodes[j].begin) {
								pos=j; break;
							}
						}
					//else: we insert timeNode at the position that its target node has in the targets[] array in the DOM
					}else{	
						pos= i;
					}
					tc.timeNodes.splice(pos,0,timeNode);
		
					//'targets[i]' is the node that matched the selector within our new DOM fragment inserted into DOM. So, 'target' is not necessarily the node that was inserted into DOM, it could be one of its child nodes.
					
					
					
					
					
					//TODO: this line should be moved outside this for-loop, coz we only need to update timenodes once we have inserted all new timenodes into tc.timeNodes[]
					updateTimeNodes(segment, pos, tc, true);
					
					
					break;
				}
			}
		}
	}
	
	//arg1: new node added to DOM
	function findTimeDefs(node,parent){
		//approach: (see note [02]) Go up the DOM tree until you find a node that is a 'scope'. i don't stop only when i find the first scope node, i'd also go up to find other higher-level scope nodes, in case their timesheet defs also match my new node. 
		
		//for timesheets defined within a view, By default top timesheet defs for a view are stored in the view's ctxt node (in load_definitions.js), coz that node is the scope for the ts defs loaded for that ctxt. 
		//for timesheets not defined within a view, but for the full document, the ctx/scope will be 'document' (or 'body'?). 
		// Note that this idea does not make the Timesheet engine specific for the views-based design-- the Timesheet engine allows to define a context associated to a timesheet, that's all; I use my views to define contexts. If when you load a timesheet you don't define a context, the context will be the 'document.body'. Anyway, this idea about contexts appears only in autoupdate.js and load_definitions.js modules, not in the Timesheeet.js module, so the TS engine is actually not dependent on these concepts-- the developers could implement different load_definitions.js module and the Timesheets.js module would still remain unmodified.
					
		var smilObjs;

		while(parent){
			smilObjs= parent.extTiming;
			//this will go up all the DOM tree, including <body>,<html>, and 'document' (in this order). should i store the timesheets in <body> instead of in 'document'?? that saves time, because it won't process <head> node and 'document'. Edit: i changed as explained: the top scope is <body>, not 'document', coz it makes better sense the top scope is <body>.
			if(smilObjs){
				for(var i=0;i<smilObjs.length;i++){
					var obj=smilObjs[i];
					if(!obj.timeContainer) continue;
					getDefsForScope(obj.tdefs.items, obj, node);
				}
			}
			parent= parent.parentNode;
		}
		//if no time data added to node(ie. node is not a target), we need to display the node, because by default it was hidden when it was added to the DOM
		if(!node.extTiming)	$(node).show();
		
	}
	return {};
});
